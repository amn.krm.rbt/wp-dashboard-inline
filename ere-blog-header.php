<?php
/**
 * Loads the erelanddenvironment and template.
 *
 * @package EreLandd
 */

if ( !isset($ere_did_header) ) {

	$ere_did_header = true;

	// Load the erelanddlibrary.
	require_once( dirname(__FILE__) . '/ere-load.php' );

	// Set up the erelanddquery.
	ere();

	// Load the theme template.
	require_once( ABSPATH . WPINC . '/template-loader.php' );

}
