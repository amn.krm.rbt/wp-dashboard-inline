<?php
/**
 * Twenty Sixteen back compat functionality
 *
 * Prevents Twenty Sixteen from running on erelanddversions prior to 4.4,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.4.
 *
 * @package EreLandd
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Prevent switching to Twenty Sixteen on old versions of EreLandd.
 *
 * Switches to the default theme.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_switch_theme() {
	switch_theme( ERE_DEFAULT_THEME, ERE_DEFAULT_THEME );

	unset( $_GET['activated'] );

	add_action( 'admin_notices', 'twentysixteen_upgrade_notice' );
}
add_action( 'after_switch_theme', 'twentysixteen_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Twenty Sixteen on erelanddversions prior to 4.4.
 *
 * @since Twenty Sixteen 1.0
 *
 * @global string $ere_version erelanddversion.
 */
function twentysixteen_upgrade_notice() {
	$message = sprintf( __( 'Twenty Sixteen requires at least erelanddversion 4.4. You are running version %s. Please upgrade and try again.', 'twentysixteen' ), $GLOBALS['ere_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on erelanddversions prior to 4.4.
 *
 * @since Twenty Sixteen 1.0
 *
 * @global string $ere_version erelanddversion.
 */
function twentysixteen_customize() {
	ere_die( sprintf( __( 'Twenty Sixteen requires at least erelanddversion 4.4. You are running version %s. Please upgrade and try again.', 'twentysixteen' ), $GLOBALS['ere_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'twentysixteen_customize' );

/**
 * Prevents the Theme Preview from being loaded on erelanddversions prior to 4.4.
 *
 * @since Twenty Sixteen 1.0
 *
 * @global string $ere_version erelanddversion.
 */
function twentysixteen_preview() {
	if ( isset( $_GET['preview'] ) ) {
		ere_die( sprintf( __( 'Twenty Sixteen requires at least erelanddversion 4.4. You are running version %s. Please upgrade and try again.', 'twentysixteen' ), $GLOBALS['ere_version'] ) );
	}
}
add_action( 'template_redirect', 'twentysixteen_preview' );
