<?php
/**
 * Bootstrap file for setting the ABSPATH constant
 * and loading the ere-config.php file. The ere-config.php
 * file will then load the ere-settings.php file, which
 * will then set up the erelanddenvironment.
 *
 * If the ere-config.php file is not found then an error
 * will be displayed asking the visitor to set up the
 * ere-config.php file.
 *
 * Will also search for ere-config.php in EreLandd' parent
 * directory to allow the erelandddirectory to remain
 * untouched.
 *
 * @internal This file must be parsable by PHP4.
 *
 * @package EreLandd
 */

/** Define ABSPATH as this file's directory */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );

/*
 * If ere-config.php exists in the erelanddroot, or if it exists in the root and ere-settings.php
 * doesn't, load ere-config.php. The secondary check for ere-settings.php has the added benefit
 * of avoiding cases where the current directory is a nested installation, e.g. / is EreLandd(a)
 * and /blog/ is EreLandd(b).
 *
 * If neither set of conditions is true, initiate loading the setup process.
 */
if ( file_exists( ABSPATH . 'ere-config.php') ) {

	/** The config file resides in ABSPATH */
	require_once( ABSPATH . 'ere-config.php' );

} elseif ( @file_exists( dirname( ABSPATH ) . '/ere-config.php' ) && ! @file_exists( dirname( ABSPATH ) . '/ere-settings.php' ) ) {

	/** The config file resides one level above ABSPATH but is not part of another install */
	require_once( dirname( ABSPATH ) . '/ere-config.php' );

} else {

	// A config file doesn't exist

	define( 'WPINC', 'ere-includes' );
	require_once( ABSPATH . WPINC . '/load.php' );

	// Standardize $_SERVER variables across setups.
	ere_fix_server_vars();

	require_once( ABSPATH . WPINC . '/functions.php' );

	$path = ere_guess_url() . '/ere-admin/setup-config.php';

	/*
	 * We're going to redirect to setup-config.php. While this shouldn't result
	 * in an infinite loop, that's a silly thing to assume, don't you think? If
	 * we're traveling in circles, our last-ditch effort is "Need more help?"
	 */
	if ( false === strpos( $_SERVER['REQUEST_URI'], 'setup-config' ) ) {
		header( 'Location: ' . $path );
		exit;
	}

	define( 'ERE_CONTENT_DIR', ABSPATH . 'ere-content' );
	require_once( ABSPATH . WPINC . '/version.php' );

	ere_check_php_mysql_versions();
	ere_load_translations_early();

	// Die with an error message
	$die  = sprintf(
		/* translators: %s: ere-config.php */
		__( "There doesn't seem to be a %s file. I need this before we can get started." ),
		'<code>ere-config.php</code>'
	) . '</p>';
	$die .= '<p>' . sprintf(
		/* translators: %s: Codex URL */
		__( "Need more help? <a href='%s'>We got it</a>." ),
		__( 'https://codex.erelandd.ir/Editing_ere-config.php' )
	) . '</p>';
	$die .= '<p>' . sprintf(
		/* translators: %s: ere-config.php */
		__( "You can create a %s file through a web interface, but this doesn't work for all server setups. The safest way is to manually create the file." ),
		'<code>ere-config.php</code>'
	) . '</p>';
	$die .= '<p><a href="' . $path . '" class="button button-large">' . __( "Create a Configuration File" ) . '</a>';

	ere_die( $die, __( 'erelandd&rsaquo; Error' ) );
}
