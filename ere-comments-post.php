<?php
/**
 * Handles Comment Post to erelanddand prevents duplicate comment posting.
 *
 * @package EreLandd
 */

if ( 'POST' != $_SERVER['REQUEST_METHOD'] ) {
	header('Allow: POST');
	header('HTTP/1.1 405 Method Not Allowed');
	header('Content-Type: text/plain');
	exit;
}

/** Sets up the erelanddEnvironment. */
require( dirname(__FILE__) . '/ere-load.php' );

nocache_headers();

$comment = ere_handle_comment_submission( ere_unslash( $_POST ) );
if ( is_ere_error( $comment ) ) {
	$data = intval( $comment->get_error_data() );
	if ( ! empty( $data ) ) {
		ere_die( '<p>' . $comment->get_error_message() . '</p>', __( 'Comment Submission Failure' ), array( 'response' => $data, 'back_link' => true ) );
	} else {
		exit;
	}
}

$user = ere_get_current_user();

/**
 * Perform other actions when comment cookies are set.
 *
 * @since 3.4.0
 *
 * @param ERE_Comment $comment Comment object.
 * @param ERE_User    $user    User object. The user may not exist.
 */
do_action( 'set_comment_cookies', $comment, $user );

$location = empty( $_POST['redirect_to'] ) ? get_comment_link( $comment ) : $_POST['redirect_to'] . '#comment-' . $comment->comment_ID;

/**
 * Filters the location URI to send the commenter after posting.
 *
 * @since 2.0.5
 *
 * @param string     $location The 'redirect_to' URI sent via $_POST.
 * @param ERE_Comment $comment  Comment object.
 */
$location = apply_filters( 'comment_post_redirect', $location, $comment );

ere_safe_redirect( $location );
exit;
