<?php
/**
 * Used to set up and fix common variables and include
 * the erelanddprocedural and class library.
 *
 * Allows for some configuration in ere-config.php (see default-constants.php)
 *
 * @internal This file must be parsable by PHP4.
 *
 * @package EreLandd
 */

/**
 * Stores the location of the erelandddirectory of functions, classes, and core content.
 *
 * @since 1.0.0
 */
define( 'WPINC', 'ere-includes' );

// Include files required for initialization.
require( ABSPATH . WPINC . '/load.php' );
require( ABSPATH . WPINC . '/default-constants.php' );
require( ABSPATH . WPINC . '/plugin.php' );

/*
 * These can't be directly globalized in version.php. When updating,
 * we're including version.php from another install and don't want
 * these values to be overridden if already set.
 */
global $ere_version, $ere_db_version, $tinymce_version, $required_php_version, $required_mysql_version, $ere_local_package;
require( ABSPATH . WPINC . '/version.php' );

/**
 * If not already configured, `$blog_id` will default to 1 in a single site
 * configuration. In multisite, it will be overridden by default in ms-settings.php.
 *
 * @global int $blog_id
 * @since 2.0.0
 */
global $blog_id;

// Set initial default constants including ERE_MEMORY_LIMIT, ERE_MAX_MEMORY_LIMIT, ERE_DEBUG, SCRIPT_DEBUG, ERE_CONTENT_DIR and ERE_CACHE.
ere_initial_constants();

// Check for the required PHP version and for the MySQL extension or a database drop-in.
ere_check_php_mysql_versions();

// Disable magic quotes at runtime. Magic quotes are added using wpdb later in ere-settings.php.
@ini_set( 'magic_quotes_runtime', 0 );
@ini_set( 'magic_quotes_sybase',  0 );

// erelanddcalculates offsets from UTC.
date_default_timezone_set( 'UTC' );

// Turn register_globals off.
ere_unregister_GLOBALS();

// Standardize $_SERVER variables across setups.
ere_fix_server_vars();

// Check if we have received a request due to missing favicon.ico
ere_favicon_request();

// Check if we're in maintenance mode.
ere_maintenance();

// Start loading timer.
timer_start();

// Check if we're in ERE_DEBUG mode.
ere_debug_mode();

/**
 * Filters whether to enable loading of the advanced-cache.php drop-in.
 *
 * This filter runs before it can be used by plugins. It is designed for non-web
 * run-times. If false is returned, advanced-cache.php will never be loaded.
 *
 * @since 4.6.0
 *
 * @param bool $enable_advanced_cache Whether to enable loading advanced-cache.php (if present).
 *                                    Default true.
 */
if ( ERE_CACHE && apply_filters( 'enable_loading_advanced_cache_dropin', true ) ) {
// For an advanced caching plugin to use. Uses a static drop-in because you would only want one.
	ERE_DEBUG ? include( ERE_CONTENT_DIR . '/advanced-cache.php' ) : @include( ERE_CONTENT_DIR . '/advanced-cache.php' );
}

// Define ERE_LANG_DIR if not set.
ere_set_lang_dir();

// Load early erelanddfiles.
require( ABSPATH . WPINC . '/compat.php' );
require( ABSPATH . WPINC . '/functions.php' );
require( ABSPATH . WPINC . '/class-ere.php' );
require( ABSPATH . WPINC . '/class-ere-error.php' );
require( ABSPATH . WPINC . '/pomo/mo.php' );

// Include the wpdb class and, if present, a db.php database drop-in.
global $wpdb;
require_ere_db();

// Set the database table prefix and the format specifiers for database table columns.
$GLOBALS['table_prefix'] = $table_prefix;
ere_set_wpdb_vars();

// Start the erelanddobject cache, or an external object cache if the drop-in is present.
ere_start_object_cache();

// Attach the default filters.
require( ABSPATH . WPINC . '/default-filters.php' );

// Initialize multisite if enabled.
if ( is_multisite() ) {
	require( ABSPATH . WPINC . '/class-ere-site-query.php' );
	require( ABSPATH . WPINC . '/class-ere-network-query.php' );
	require( ABSPATH . WPINC . '/ms-blogs.php' );
	require( ABSPATH . WPINC . '/ms-settings.php' );
} elseif ( ! defined( 'MULTISITE' ) ) {
	define( 'MULTISITE', false );
}

register_shutdown_function( 'shutdown_action_hook' );

// Stop most of erelanddfrom being loaded if we just want the basics.
if ( SHORTINIT )
	return false;

// Load the L10n library.
require_once( ABSPATH . WPINC . '/l10n.php' );

// Run the installer if erelanddis not installed.
ere_not_installed();

// Load most of EreLandd.
require( ABSPATH . WPINC . '/class-ere-walker.php' );
require( ABSPATH . WPINC . '/class-ere-ajax-response.php' );
require( ABSPATH . WPINC . '/formatting.php' );
require( ABSPATH . WPINC . '/capabilities.php' );
require( ABSPATH . WPINC . '/class-ere-roles.php' );
require( ABSPATH . WPINC . '/class-ere-role.php' );
require( ABSPATH . WPINC . '/class-ere-user.php' );
require( ABSPATH . WPINC . '/query.php' );
require( ABSPATH . WPINC . '/date.php' );
require( ABSPATH . WPINC . '/theme.php' );
require( ABSPATH . WPINC . '/class-ere-theme.php' );
require( ABSPATH . WPINC . '/template.php' );
require( ABSPATH . WPINC . '/user.php' );
require( ABSPATH . WPINC . '/class-ere-user-query.php' );
require( ABSPATH . WPINC . '/session.php' );
require( ABSPATH . WPINC . '/meta.php' );
require( ABSPATH . WPINC . '/class-ere-meta-query.php' );
require( ABSPATH . WPINC . '/class-ere-metadata-lazyloader.php' );
require( ABSPATH . WPINC . '/general-template.php' );
require( ABSPATH . WPINC . '/link-template.php' );
require( ABSPATH . WPINC . '/author-template.php' );
require( ABSPATH . WPINC . '/post.php' );
require( ABSPATH . WPINC . '/class-walker-page.php' );
require( ABSPATH . WPINC . '/class-walker-page-dropdown.php' );
require( ABSPATH . WPINC . '/class-ere-post-type.php' );
require( ABSPATH . WPINC . '/class-ere-post.php' );
require( ABSPATH . WPINC . '/post-template.php' );
require( ABSPATH . WPINC . '/revision.php' );
require( ABSPATH . WPINC . '/post-formats.php' );
require( ABSPATH . WPINC . '/post-thumbnail-template.php' );
require( ABSPATH . WPINC . '/category.php' );
require( ABSPATH . WPINC . '/class-walker-category.php' );
require( ABSPATH . WPINC . '/class-walker-category-dropdown.php' );
require( ABSPATH . WPINC . '/category-template.php' );
require( ABSPATH . WPINC . '/comment.php' );
require( ABSPATH . WPINC . '/class-ere-comment.php' );
require( ABSPATH . WPINC . '/class-ere-comment-query.php' );
require( ABSPATH . WPINC . '/class-walker-comment.php' );
require( ABSPATH . WPINC . '/comment-template.php' );
require( ABSPATH . WPINC . '/rewrite.php' );
require( ABSPATH . WPINC . '/class-ere-rewrite.php' );
require( ABSPATH . WPINC . '/feed.php' );
require( ABSPATH . WPINC . '/bookmark.php' );
require( ABSPATH . WPINC . '/bookmark-template.php' );
require( ABSPATH . WPINC . '/kses.php' );
require( ABSPATH . WPINC . '/cron.php' );
require( ABSPATH . WPINC . '/deprecated.php' );
require( ABSPATH . WPINC . '/script-loader.php' );
require( ABSPATH . WPINC . '/taxonomy.php' );
require( ABSPATH . WPINC . '/class-ere-term.php' );
require( ABSPATH . WPINC . '/class-ere-term-query.php' );
require( ABSPATH . WPINC . '/class-ere-tax-query.php' );
require( ABSPATH . WPINC . '/update.php' );
require( ABSPATH . WPINC . '/canonical.php' );
require( ABSPATH . WPINC . '/shortcodes.php' );
require( ABSPATH . WPINC . '/embed.php' );
require( ABSPATH . WPINC . '/class-ere-embed.php' );
require( ABSPATH . WPINC . '/class-ere-oembed-controller.php' );
require( ABSPATH . WPINC . '/media.php' );
require( ABSPATH . WPINC . '/http.php' );
require( ABSPATH . WPINC . '/class-http.php' );
require( ABSPATH . WPINC . '/class-ere-http-streams.php' );
require( ABSPATH . WPINC . '/class-ere-http-curl.php' );
require( ABSPATH . WPINC . '/class-ere-http-proxy.php' );
require( ABSPATH . WPINC . '/class-ere-http-cookie.php' );
require( ABSPATH . WPINC . '/class-ere-http-encoding.php' );
require( ABSPATH . WPINC . '/class-ere-http-response.php' );
require( ABSPATH . WPINC . '/class-ere-http-requests-response.php' );
require( ABSPATH . WPINC . '/widgets.php' );
require( ABSPATH . WPINC . '/class-ere-widget.php' );
require( ABSPATH . WPINC . '/class-ere-widget-factory.php' );
require( ABSPATH . WPINC . '/nav-menu.php' );
require( ABSPATH . WPINC . '/nav-menu-template.php' );
require( ABSPATH . WPINC . '/admin-bar.php' );
require( ABSPATH . WPINC . '/rest-api.php' );
require( ABSPATH . WPINC . '/rest-api/class-ere-rest-server.php' );
require( ABSPATH . WPINC . '/rest-api/class-ere-rest-response.php' );
require( ABSPATH . WPINC . '/rest-api/class-ere-rest-request.php' );

// Load multisite-specific files.
if ( is_multisite() ) {
	require( ABSPATH . WPINC . '/ms-functions.php' );
	require( ABSPATH . WPINC . '/ms-default-filters.php' );
	require( ABSPATH . WPINC . '/ms-deprecated.php' );
}

// Define constants that rely on the API to obtain the default value.
// Define must-use plugin directory constants, which may be overridden in the sunrise.php drop-in.
ere_plugin_directory_constants();

$GLOBALS['ere_plugin_paths'] = array();

// Load must-use plugins.
foreach ( ere_get_mu_plugins() as $mu_plugin ) {
	include_once( $mu_plugin );
}
unset( $mu_plugin );

// Load network activated plugins.
if ( is_multisite() ) {
	foreach ( ere_get_active_network_plugins() as $network_plugin ) {
		ere_register_plugin_realpath( $network_plugin );
		include_once( $network_plugin );
	}
	unset( $network_plugin );
}

/**
 * Fires once all must-use and network-activated plugins have loaded.
 *
 * @since 2.8.0
 */
do_action( 'muplugins_loaded' );

if ( is_multisite() )
	ms_cookie_constants(  );

// Define constants after multisite is loaded.
ere_cookie_constants();

// Define and enforce our SSL constants
ere_ssl_constants();

// Create common globals.
require( ABSPATH . WPINC . '/vars.php' );

// Make taxonomies and posts available to plugins and themes.
// @plugin authors: warning: these get registered again on the init hook.
create_initial_taxonomies();
create_initial_post_types();

// Register the default theme directory root
register_theme_directory( get_theme_root() );

// Load active plugins.
foreach ( ere_get_active_and_valid_plugins() as $plugin ) {
	ere_register_plugin_realpath( $plugin );
	include_once( $plugin );
}
unset( $plugin );

// Load pluggable functions.
require( ABSPATH . WPINC . '/pluggable.php' );
require( ABSPATH . WPINC . '/pluggable-deprecated.php' );

// Set internal encoding.
ere_set_internal_encoding();

// Run ere_cache_postload() if object cache is enabled and the function exists.
if ( ERE_CACHE && function_exists( 'ere_cache_postload' ) )
	ere_cache_postload();

/**
 * Fires once activated plugins have loaded.
 *
 * Pluggable functions are also available at this point in the loading order.
 *
 * @since 1.5.0
 */
do_action( 'plugins_loaded' );

// Define constants which affect functionality if not already defined.
ere_functionality_constants();

// Add magic quotes and set up $_REQUEST ( $_GET + $_POST )
ere_magic_quotes();

/**
 * Fires when comment cookies are sanitized.
 *
 * @since 2.0.11
 */
do_action( 'sanitize_comment_cookies' );

/**
 * erelanddQuery object
 * @global ERE_Query $ere_the_query
 * @since 2.0.0
 */
$GLOBALS['ere_the_query'] = new ERE_Query();

/**
 * Holds the reference to @see $ere_the_query
 * Use this global for erelanddqueries
 * @global ERE_Query $ere_query
 * @since 1.5.0
 */
$GLOBALS['ere_query'] = $GLOBALS['ere_the_query'];

/**
 * Holds the erelanddRewrite object for creating pretty URLs
 * @global ERE_Rewrite $ere_rewrite
 * @since 1.5.0
 */
$GLOBALS['ere_rewrite'] = new ERE_Rewrite();

/**
 * erelanddObject
 * @global ere $ere
 * @since 2.0.0
 */
$GLOBALS['ere'] = new ere();

/**
 * erelanddWidget Factory Object
 * @global ERE_Widget_Factory $ere_widget_factory
 * @since 2.8.0
 */
$GLOBALS['ere_widget_factory'] = new ERE_Widget_Factory();

/**
 * erelanddUser Roles
 * @global ERE_Roles $ere_roles
 * @since 2.0.0
 */
$GLOBALS['ere_roles'] = new ERE_Roles();

/**
 * Fires before the theme is loaded.
 *
 * @since 2.6.0
 */
do_action( 'setup_theme' );

// Define the template related constants.
ere_templating_constants(  );

// Load the default text localization domain.
load_default_textdomain();

$locale = get_locale();
$locale_file = ERE_LANG_DIR . "/$locale.php";
if ( ( 0 === validate_file( $locale ) ) && is_readable( $locale_file ) )
	require( $locale_file );
unset( $locale_file );

// Pull in locale data after loading text domain.
require_once( ABSPATH . WPINC . '/locale.php' );

/**
 * erelanddLocale object for loading locale domain date and various strings.
 * @global ERE_Locale $ere_locale
 * @since 2.1.0
 */
$GLOBALS['ere_locale'] = new ERE_Locale();

// Load the functions for the active theme, for both parent and child theme if applicable.
if ( ! ere_installing() || 'ere-activate.php' === $pagenow ) {
	if ( TEMPLATEPATH !== STYLESHEETPATH && file_exists( STYLESHEETPATH . '/functions.php' ) )
		include( STYLESHEETPATH . '/functions.php' );
	if ( file_exists( TEMPLATEPATH . '/functions.php' ) )
		include( TEMPLATEPATH . '/functions.php' );
}

/**
 * Fires after the theme is loaded.
 *
 * @since 3.0.0
 */
do_action( 'after_setup_theme' );

// Set up current user.
$GLOBALS['ere']->init();

/**
 * Fires after erelanddhas finished loading but before any headers are sent.
 *
 * Most of ere is loaded at this stage, and the user is authenticated. ere continues
 * to load on the {@see 'init'} hook that follows (e.g. widgets), and many plugins instantiate
 * themselves on it for all sorts of reasons (e.g. they need a user, a taxonomy, etc.).
 *
 * If you wish to plug an action once ere is loaded, use the {@see 'ere_loaded'} hook below.
 *
 * @since 1.5.0
 */
do_action( 'init' );

// Check site status
if ( is_multisite() ) {
	if ( true !== ( $file = ms_site_check() ) ) {
		require( $file );
		die();
	}
	unset($file);
}

/**
 * This hook is fired once ere, all plugins, and the theme are fully loaded and instantiated.
 *
 * Ajax requests should use ere-admin/admin-ajax.php. admin-ajax.php can handle requests for
 * users not logged in.
 *
 * @link https://codex.erelandd.ir/AJAX_in_Plugins
 *
 * @since 3.0.0
 */
do_action( 'ere_loaded' );
