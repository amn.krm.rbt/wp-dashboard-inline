<?php
/**
 * Locale API
 *
 * @package EreLandd
 * @subpackage i18n
 * @since 1.2.0
 */

/** ERE_Locale class */
require_once ABSPATH . WPINC . '/class-ere-locale.php';

/**
 * Checks if current locale is RTL.
 *
 * @since 3.0.0
 *
 * @global ERE_Locale $ere_locale
 *
 * @return bool Whether locale is RTL.
 */
function is_rtl() {
	global $ere_locale;
	return $ere_locale->is_rtl();
}
