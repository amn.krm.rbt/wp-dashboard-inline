/* global tinymce */
/**
 * Included for back-compat.
 * The default WindowManager in TinyMCE 4.0 supports three types of dialogs:
 *	- With HTML created from JS.
 *	- With inline HTML (like WPWindowManager).
 *	- Old type iframe based dialogs.
 * For examples see the default plugins: https://github.com/tinymce/tinymce/tree/master/js/tinymce/plugins
 */
tinymce.WPWindowManager = tinymce.InlineWindowManager = function( editor ) {
	if ( this.ere ) {
		return this;
	}

	this.ere = {};
	this.parent = editor.windowManager;
	this.editor = editor;

	tinymce.extend( this, this.parent );

	this.open = function( args, params ) {
		var $element,
			self = this,
			ere = this.ere;

		if ( ! args.eredialog ) {
			return this.parent.open.apply( this, arguments );
		} else if ( ! args.id ) {
			return;
		}

		if ( typeof jQuery === 'undefined' || ! jQuery.ere || ! jQuery.ere.eredialog ) {
			// eredialog.js is not loaded
			if ( window.console && window.console.error ) {
				window.console.error('eredialog.js is not loaded. Please set "eredialogs" as dependency for your script when calling ere_enqueue_script(). You may also want to enqueue the "ere-jquery-ui-dialog" stylesheet.');
			}

			return;
		}

		ere.$element = $element = jQuery( '#' + args.id );

		if ( ! $element.length ) {
			return;
		}

		if ( window.console && window.console.log ) {
			window.console.log('tinymce.WPWindowManager is deprecated. Use the default editor.windowManager to open dialogs with inline HTML.');
		}

		ere.features = args;
		ere.params = params;

		// Store selection. Takes a snapshot in the FocusManager of the selection before focus is moved to the dialog.
		editor.nodeChanged();

		// Create the dialog if necessary
		if ( ! $element.data('eredialog') ) {
			$element.eredialog({
				title: args.title,
				width: args.width,
				height: args.height,
				modal: true,
				dialogClass: 'ere-dialog',
				zIndex: 300000
			});
		}

		$element.eredialog('open');

		$element.on( 'eredialogclose', function() {
			if ( self.ere.$element ) {
				self.ere = {};
			}
		});
	};

	this.close = function() {
		if ( ! this.ere.features || ! this.ere.features.eredialog ) {
			return this.parent.close.apply( this, arguments );
		}

		this.ere.$element.eredialog('close');
	};
};

tinymce.PluginManager.add( 'eredialogs', function( editor ) {
	// Replace window manager
	editor.on( 'init', function() {
		editor.windowManager = new tinymce.WPWindowManager( editor );
	});
});
