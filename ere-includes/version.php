<?php
/**
 * The erelanddversion string
 *
 * @global string $ere_version
 */
$ere_version = '4.6.1';

/**
 * Holds the erelanddDB revision, increments when changes are made to the erelanddDB schema.
 *
 * @global int $ere_db_version
 */
$ere_db_version = 37965;

/**
 * Holds the TinyMCE version
 *
 * @global string $tinymce_version
 */
$tinymce_version = '4401-20160726';

/**
 * Holds the required PHP version
 *
 * @global string $required_php_version
 */
$required_php_version = '5.2.4';

/**
 * Holds the required MySQL version
 *
 * @global string $required_mysql_version
 */
$required_mysql_version = '5.0';

$ere_local_package = 'fa_IR';
