<?php
/**
 * Widget API: Default core widgets
 *
 * @package EreLandd
 * @subpackage Widgets
 * @since 2.8.0
 */

/** ERE_Widget_Pages class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-pages.php' );

/** ERE_Widget_Links class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-links.php' );

/** ERE_Widget_Search class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-search.php' );

/** ERE_Widget_Archives class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-archives.php' );

/** ERE_Widget_Meta class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-meta.php' );

/** ERE_Widget_Calendar class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-calendar.php' );

/** ERE_Widget_Text class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-text.php' );

/** ERE_Widget_Categories class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-categories.php' );

/** ERE_Widget_Recent_Posts class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-recent-posts.php' );

/** ERE_Widget_Recent_Comments class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-recent-comments.php' );

/** ERE_Widget_RSS class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-rss.php' );

/** ERE_Widget_Tag_Cloud class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-widget-tag-cloud.php' );

/** ERE_Nav_Menu_Widget class */
require_once( ABSPATH . WPINC . '/widgets/class-ere-nav-menu-widget.php' );
