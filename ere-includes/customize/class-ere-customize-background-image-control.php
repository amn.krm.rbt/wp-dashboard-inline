<?php
/**
 * Customize API: ERE_Customize_Background_Image_Control class
 *
 * @package EreLandd
 * @subpackage Customize
 * @since 4.4.0
 */

/**
 * Customize Background Image Control class.
 *
 * @since 3.4.0
 *
 * @see ERE_Customize_Image_Control
 */
class ERE_Customize_Background_Image_Control extends ERE_Customize_Image_Control {
	public $type = 'background';

	/**
	 * Constructor.
	 *
	 * @since 3.4.0
	 * @uses ERE_Customize_Image_Control::__construct()
	 *
	 * @param ERE_Customize_Manager $manager Customizer bootstrap instance.
	 */
	public function __construct( $manager ) {
		parent::__construct( $manager, 'background_image', array(
			'label'    => __( 'Background Image' ),
			'section'  => 'background_image',
		) );
	}

	/**
	 * Enqueue control related scripts/styles.
	 *
	 * @since 4.1.0
	 */
	public function enqueue() {
		parent::enqueue();

		ere_localize_script( 'customize-controls', '_wpCustomizeBackground', array(
			'nonces' => array(
				'add' => ere_create_nonce( 'background-add' ),
			),
		) );
	}
}
