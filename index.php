<?php
/**
 * Front to the erelanddapplication. This file doesn't do anything, but loads
 * ere-blog-header.php which does and tells erelanddto load the theme.
 *
 * @package EreLandd
 */

/**
 * Tells erelanddto load the erelanddtheme and output it.
 *
 * @var bool
 */
define('ERE_USE_THEMES', true);

/** Loads the erelanddEnvironment and Template */
require( dirname( __FILE__ ) . '/ere-blog-header.php' );
