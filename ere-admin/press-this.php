<?php
/**
 * Press This Display and Handler.
 *
 * @package EreLandd
 * @subpackage Press_This
 */

define('IFRAME_REQUEST' , true);

/** erelanddAdministration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! current_user_can( 'edit_posts' ) || ! current_user_can( get_post_type_object( 'post' )->cap->create_posts ) ) {
	ere_die(
		'<h1>' . __( 'Cheatin&#8217; uh?' ) . '</h1>' .
		'<p>' . __( 'Sorry, you are not allowed to create posts as this user.' ) . '</p>',
		403
	);
}

/**
 * @global ERE_Press_This $ere_press_this
 */
if ( empty( $GLOBALS['ere_press_this'] ) ) {
	include( ABSPATH . 'ere-admin/includes/class-ere-press-this.php' );
}

$GLOBALS['ere_press_this']->html();
