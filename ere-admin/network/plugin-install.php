<?php
/**
 * Install plugin network administration panel.
 *
 * @package EreLandd
 * @subpackage Multisite
 * @since 3.1.0
 */

if ( isset( $_GET['tab'] ) && ( 'plugin-information' == $_GET['tab'] ) )
	define( 'IFRAME_REQUEST', true );

/** Load erelanddAdministration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! is_multisite() )
	ere_die( __( 'Multisite support is not enabled.' ) );

require( ABSPATH . 'ere-admin/plugin-install.php' );
