<?php
/**
 * Network Freedoms administration panel.
 *
 * @package EreLandd
 * @subpackage Multisite
 * @since 3.4.0
 */

/** Load erelanddAdministration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! is_multisite() )
	ere_die( __( 'Multisite support is not enabled.' ) );

require( ABSPATH . 'ere-admin/freedoms.php' );
