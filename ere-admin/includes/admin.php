<?php
/**
 * Core Administration API
 *
 * @package EreLandd
 * @subpackage Administration
 * @since 2.3.0
 */

if ( ! defined('ERE_ADMIN') ) {
	/*
	 * This file is being included from a file other than ere-admin/admin.php, so
	 * some setup was skipped. Make sure the admin message catalog is loaded since
	 * load_default_textdomain() will not have done so in this context.
	 */
	load_textdomain( 'default', ERE_LANG_DIR . '/admin-' . get_locale() . '.mo' );
}

/** erelanddAdministration Hooks */
require_once(ABSPATH . 'ere-admin/includes/admin-filters.php');

/** erelanddBookmark Administration API */
require_once(ABSPATH . 'ere-admin/includes/bookmark.php');

/** erelanddComment Administration API */
require_once(ABSPATH . 'ere-admin/includes/comment.php');

/** erelanddAdministration File API */
require_once(ABSPATH . 'ere-admin/includes/file.php');

/** erelanddImage Administration API */
require_once(ABSPATH . 'ere-admin/includes/image.php');

/** erelanddMedia Administration API */
require_once(ABSPATH . 'ere-admin/includes/media.php');

/** erelanddImport Administration API */
require_once(ABSPATH . 'ere-admin/includes/import.php');

/** erelanddMisc Administration API */
require_once(ABSPATH . 'ere-admin/includes/misc.php');

/** erelanddOptions Administration API */
require_once(ABSPATH . 'ere-admin/includes/options.php');

/** erelanddPlugin Administration API */
require_once(ABSPATH . 'ere-admin/includes/plugin.php');

/** erelanddPost Administration API */
require_once(ABSPATH . 'ere-admin/includes/post.php');

/** erelanddAdministration Screen API */
require_once(ABSPATH . 'ere-admin/includes/class-ere-screen.php');
require_once(ABSPATH . 'ere-admin/includes/screen.php');

/** erelanddTaxonomy Administration API */
require_once(ABSPATH . 'ere-admin/includes/taxonomy.php');

/** erelanddTemplate Administration API */
require_once(ABSPATH . 'ere-admin/includes/template.php');

/** erelanddList Table Administration API and base class */
require_once(ABSPATH . 'ere-admin/includes/class-ere-list-table.php');
require_once(ABSPATH . 'ere-admin/includes/list-table.php');

/** erelanddTheme Administration API */
require_once(ABSPATH . 'ere-admin/includes/theme.php');

/** erelanddUser Administration API */
require_once(ABSPATH . 'ere-admin/includes/user.php');

/** erelanddSite Icon API */
require_once(ABSPATH . 'ere-admin/includes/class-ere-site-icon.php');

/** erelanddUpdate Administration API */
require_once(ABSPATH . 'ere-admin/includes/update.php');

/** erelanddDeprecated Administration API */
require_once(ABSPATH . 'ere-admin/includes/deprecated.php');

/** erelanddMultisite support API */
if ( is_multisite() ) {
	require_once(ABSPATH . 'ere-admin/includes/ms-admin-filters.php');
	require_once(ABSPATH . 'ere-admin/includes/ms.php');
	require_once(ABSPATH . 'ere-admin/includes/ms-deprecated.php');
}
