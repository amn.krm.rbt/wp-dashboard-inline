<?php
/**
 * erelanddCredits Administration API.
 *
 * @package EreLandd
 * @subpackage Administration
 * @since 4.4.0
 */

/**
 * Retrieve the contributor credits.
 *
 * @global string $ere_version The current erelanddversion.
 *
 * @since 3.2.0
 *
 * @return array|false A list of all of the contributors, or false on error.
 */
function ere_credits() {
	global $ere_version;
	$locale = get_locale();

	$results = get_site_transient( 'erelandd_credits_' . $locale );

	if ( ! is_array( $results )
		|| false !== strpos( $ere_version, '-' )
		|| ( isset( $results['data']['version'] ) && strpos( $ere_version, $results['data']['version'] ) !== 0 )
	) {
		$response = ere_remote_get( "http://api.erelandd.ir/core/credits/1.1/?version=$ere_version&locale=$locale" );

		if ( is_ere_error( $response ) || 200 != ere_remote_retrieve_response_code( $response ) )
			return false;

		$results = json_decode( ere_remote_retrieve_body( $response ), true );

		if ( ! is_array( $results ) )
			return false;

		set_site_transient( 'erelandd_credits_' . $locale, $results, DAY_IN_SECONDS );
	}

	return $results;
}

/**
 * Retrieve the link to a contributor's erelandd.ir profile page.
 *
 * @access private
 * @since 3.2.0
 *
 * @param string $display_name  The contributor's display name, passed by reference.
 * @param string $username      The contributor's username.
 * @param string $profiles      URL to the contributor's erelandd.ir profile page.
 */
function _ere_credits_add_profile_link( &$display_name, $username, $profiles ) {
	$display_name = '<a href="' . esc_url( sprintf( $profiles, $username ) ) . '">' . esc_html( $display_name ) . '</a>';
}

/**
 * Retrieve the link to an external library used in EreLandd.
 *
 * @access private
 * @since 3.2.0
 *
 * @param string $data External library data, passed by reference.
 */
function _ere_credits_build_object_link( &$data ) {
	$data = '<a href="' . esc_url( $data[1] ) . '">' . esc_html( $data[0] ) . '</a>';
}
