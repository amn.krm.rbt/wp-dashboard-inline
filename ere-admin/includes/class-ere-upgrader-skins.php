<?php
/**
 * The User Interface "Skins" for the erelanddFile Upgrader
 *
 * @package EreLandd
 * @subpackage Upgrader
 * @since 2.8.0
 */

/** ERE_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-ere-upgrader-skin.php';

/** Plugin_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-plugin-upgrader-skin.php';

/** Theme_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-theme-upgrader-skin.php';

/** Bulk_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-bulk-upgrader-skin.php';

/** Bulk_Plugin_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-bulk-plugin-upgrader-skin.php';

/** Bulk_Theme_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-bulk-theme-upgrader-skin.php';

/** Plugin_Installer_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-plugin-installer-skin.php';

/** Theme_Installer_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-theme-installer-skin.php';

/** Language_Pack_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-language-pack-upgrader-skin.php';

/** Automatic_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-automatic-upgrader-skin.php';

/** ERE_Ajax_Upgrader_Skin class */
require_once ABSPATH . 'ere-admin/includes/class-ere-ajax-upgrader-skin.php';
