<?php
/**
 * erelanddcore upgrade functionality.
 *
 * @package EreLandd
 * @subpackage Administration
 * @since 2.7.0
 */

/**
 * Stores files to be deleted.
 *
 * @since 2.7.0
 * @global array $_old_files
 * @var array
 * @name $_old_files
 */
global $_old_files;

$_old_files = array(
// 2.0
'ere-admin/import-b2.php',
'ere-admin/import-blogger.php',
'ere-admin/import-greymatter.php',
'ere-admin/import-livejournal.php',
'ere-admin/import-mt.php',
'ere-admin/import-rss.php',
'ere-admin/import-textpattern.php',
'ere-admin/quicktags.js',
'ere-images/fade-butt.png',
'ere-images/get-firefox.png',
'ere-images/header-shadow.png',
'ere-images/smilies',
'ere-images/ere-small.png',
'ere-images/wpminilogo.png',
'ere.php',
// 2.0.8
'ere-includes/js/tinymce/plugins/inlinepopups/readme.txt',
// 2.1
'ere-admin/edit-form-ajax-cat.php',
'ere-admin/execute-pings.php',
'ere-admin/inline-uploading.php',
'ere-admin/link-categories.php',
'ere-admin/list-manipulation.js',
'ere-admin/list-manipulation.php',
'ere-includes/comment-functions.php',
'ere-includes/feed-functions.php',
'ere-includes/functions-compat.php',
'ere-includes/functions-formatting.php',
'ere-includes/functions-post.php',
'ere-includes/js/dbx-key.js',
'ere-includes/js/tinymce/plugins/autosave/langs/cs.js',
'ere-includes/js/tinymce/plugins/autosave/langs/sv.js',
'ere-includes/links.php',
'ere-includes/pluggable-functions.php',
'ere-includes/template-functions-author.php',
'ere-includes/template-functions-category.php',
'ere-includes/template-functions-general.php',
'ere-includes/template-functions-links.php',
'ere-includes/template-functions-post.php',
'ere-includes/ere-l10n.php',
// 2.2
'ere-admin/cat-js.php',
'ere-admin/import/b2.php',
'ere-includes/js/autosave-js.php',
'ere-includes/js/list-manipulation-js.php',
'ere-includes/js/ere-ajax-js.php',
// 2.3
'ere-admin/admin-db.php',
'ere-admin/cat.js',
'ere-admin/categories.js',
'ere-admin/custom-fields.js',
'ere-admin/dbx-admin-key.js',
'ere-admin/edit-comments.js',
'ere-admin/install-rtl.css',
'ere-admin/install.css',
'ere-admin/upgrade-schema.php',
'ere-admin/upload-functions.php',
'ere-admin/upload-rtl.css',
'ere-admin/upload.css',
'ere-admin/upload.js',
'ere-admin/users.js',
'ere-admin/widgets-rtl.css',
'ere-admin/widgets.css',
'ere-admin/xfn.js',
'ere-includes/js/tinymce/license.html',
// 2.5
'ere-admin/css/upload.css',
'ere-admin/images/box-bg-left.gif',
'ere-admin/images/box-bg-right.gif',
'ere-admin/images/box-bg.gif',
'ere-admin/images/box-butt-left.gif',
'ere-admin/images/box-butt-right.gif',
'ere-admin/images/box-butt.gif',
'ere-admin/images/box-head-left.gif',
'ere-admin/images/box-head-right.gif',
'ere-admin/images/box-head.gif',
'ere-admin/images/heading-bg.gif',
'ere-admin/images/login-bkg-bottom.gif',
'ere-admin/images/login-bkg-tile.gif',
'ere-admin/images/notice.gif',
'ere-admin/images/toggle.gif',
'ere-admin/includes/upload.php',
'ere-admin/js/dbx-admin-key.js',
'ere-admin/js/link-cat.js',
'ere-admin/profile-update.php',
'ere-admin/templates.php',
'ere-includes/images/wlw/WpComments.png',
'ere-includes/images/wlw/WpIcon.png',
'ere-includes/images/wlw/WpWatermark.png',
'ere-includes/js/dbx.js',
'ere-includes/js/fat.js',
'ere-includes/js/list-manipulation.js',
'ere-includes/js/tinymce/langs/en.js',
'ere-includes/js/tinymce/plugins/autosave/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/autosave/langs',
'ere-includes/js/tinymce/plugins/directionality/images',
'ere-includes/js/tinymce/plugins/directionality/langs',
'ere-includes/js/tinymce/plugins/inlinepopups/css',
'ere-includes/js/tinymce/plugins/inlinepopups/images',
'ere-includes/js/tinymce/plugins/inlinepopups/jscripts',
'ere-includes/js/tinymce/plugins/paste/images',
'ere-includes/js/tinymce/plugins/paste/jscripts',
'ere-includes/js/tinymce/plugins/paste/langs',
'ere-includes/js/tinymce/plugins/spellchecker/classes/HttpClient.class.php',
'ere-includes/js/tinymce/plugins/spellchecker/classes/TinyGoogleSpell.class.php',
'ere-includes/js/tinymce/plugins/spellchecker/classes/TinyPspell.class.php',
'ere-includes/js/tinymce/plugins/spellchecker/classes/TinyPspellShell.class.php',
'ere-includes/js/tinymce/plugins/spellchecker/css/spellchecker.css',
'ere-includes/js/tinymce/plugins/spellchecker/images',
'ere-includes/js/tinymce/plugins/spellchecker/langs',
'ere-includes/js/tinymce/plugins/spellchecker/tinyspell.php',
'ere-includes/js/tinymce/plugins/erelandd/images',
'ere-includes/js/tinymce/plugins/erelandd/langs',
'ere-includes/js/tinymce/plugins/erelandd/erelandd.css',
'ere-includes/js/tinymce/plugins/wphelp',
'ere-includes/js/tinymce/themes/advanced/css',
'ere-includes/js/tinymce/themes/advanced/images',
'ere-includes/js/tinymce/themes/advanced/jscripts',
'ere-includes/js/tinymce/themes/advanced/langs',
// 2.5.1
'ere-includes/js/tinymce/tiny_mce_gzip.php',
// 2.6
'ere-admin/bookmarklet.php',
'ere-includes/js/jquery/jquery.dimensions.min.js',
'ere-includes/js/tinymce/plugins/erelandd/popups.css',
'ere-includes/js/ere-ajax.js',
// 2.7
'ere-admin/css/press-this-ie-rtl.css',
'ere-admin/css/press-this-ie.css',
'ere-admin/css/upload-rtl.css',
'ere-admin/edit-form.php',
'ere-admin/images/comment-pill.gif',
'ere-admin/images/comment-stalk-classic.gif',
'ere-admin/images/comment-stalk-fresh.gif',
'ere-admin/images/comment-stalk-rtl.gif',
'ere-admin/images/del.png',
'ere-admin/images/gear.png',
'ere-admin/images/media-button-gallery.gif',
'ere-admin/images/media-buttons.gif',
'ere-admin/images/postbox-bg.gif',
'ere-admin/images/tab.png',
'ere-admin/images/tail.gif',
'ere-admin/js/forms.js',
'ere-admin/js/upload.js',
'ere-admin/link-import.php',
'ere-includes/images/audio.png',
'ere-includes/images/css.png',
'ere-includes/images/default.png',
'ere-includes/images/doc.png',
'ere-includes/images/exe.png',
'ere-includes/images/html.png',
'ere-includes/images/js.png',
'ere-includes/images/pdf.png',
'ere-includes/images/swf.png',
'ere-includes/images/tar.png',
'ere-includes/images/text.png',
'ere-includes/images/video.png',
'ere-includes/images/zip.png',
'ere-includes/js/tinymce/tiny_mce_config.php',
'ere-includes/js/tinymce/tiny_mce_ext.js',
// 2.8
'ere-admin/js/users.js',
'ere-includes/js/swfupload/plugins/swfupload.documentready.js',
'ere-includes/js/swfupload/plugins/swfupload.graceful_degradation.js',
'ere-includes/js/swfupload/swfupload_f9.swf',
'ere-includes/js/tinymce/plugins/autosave',
'ere-includes/js/tinymce/plugins/paste/css',
'ere-includes/js/tinymce/utils/mclayer.js',
'ere-includes/js/tinymce/erelandd.css',
// 2.8.5
'ere-admin/import/btt.php',
'ere-admin/import/jkw.php',
// 2.9
'ere-admin/js/page.dev.js',
'ere-admin/js/page.js',
'ere-admin/js/set-post-thumbnail-handler.dev.js',
'ere-admin/js/set-post-thumbnail-handler.js',
'ere-admin/js/slug.dev.js',
'ere-admin/js/slug.js',
'ere-includes/gettext.php',
'ere-includes/js/tinymce/plugins/erelandd/js',
'ere-includes/streams.php',
// MU
'README.txt',
'htaccess.dist',
'index-install.php',
'ere-admin/css/mu-rtl.css',
'ere-admin/css/mu.css',
'ere-admin/images/site-admin.png',
'ere-admin/includes/mu.php',
'ere-admin/wpmu-admin.php',
'ere-admin/wpmu-blogs.php',
'ere-admin/wpmu-edit.php',
'ere-admin/wpmu-options.php',
'ere-admin/wpmu-themes.php',
'ere-admin/wpmu-upgrade-site.php',
'ere-admin/wpmu-users.php',
'ere-includes/images/erelandd-mu.png',
'ere-includes/wpmu-default-filters.php',
'ere-includes/wpmu-functions.php',
'wpmu-settings.php',
// 3.0
'ere-admin/categories.php',
'ere-admin/edit-category-form.php',
'ere-admin/edit-page-form.php',
'ere-admin/edit-pages.php',
'ere-admin/images/admin-header-footer.png',
'ere-admin/images/browse-happy.gif',
'ere-admin/images/ico-add.png',
'ere-admin/images/ico-close.png',
'ere-admin/images/ico-edit.png',
'ere-admin/images/ico-viewpage.png',
'ere-admin/images/fav-top.png',
'ere-admin/images/screen-options-left.gif',
'ere-admin/images/ere-logo-vs.gif',
'ere-admin/images/ere-logo.gif',
'ere-admin/import',
'ere-admin/js/ere-gears.dev.js',
'ere-admin/js/ere-gears.js',
'ere-admin/options-misc.php',
'ere-admin/page-new.php',
'ere-admin/page.php',
'ere-admin/rtl.css',
'ere-admin/rtl.dev.css',
'ere-admin/update-links.php',
'ere-admin/ere-admin.css',
'ere-admin/ere-admin.dev.css',
'ere-includes/js/codepress',
'ere-includes/js/codepress/engines/khtml.js',
'ere-includes/js/codepress/engines/older.js',
'ere-includes/js/jquery/autocomplete.dev.js',
'ere-includes/js/jquery/autocomplete.js',
'ere-includes/js/jquery/interface.js',
'ere-includes/js/scriptaculous/prototype.js',
'ere-includes/js/tinymce/ere-tinymce.js',
// 3.1
'ere-admin/edit-attachment-rows.php',
'ere-admin/edit-link-categories.php',
'ere-admin/edit-link-category-form.php',
'ere-admin/edit-post-rows.php',
'ere-admin/images/button-grad-active-vs.png',
'ere-admin/images/button-grad-vs.png',
'ere-admin/images/fav-arrow-vs-rtl.gif',
'ere-admin/images/fav-arrow-vs.gif',
'ere-admin/images/fav-top-vs.gif',
'ere-admin/images/list-vs.png',
'ere-admin/images/screen-options-right-up.gif',
'ere-admin/images/screen-options-right.gif',
'ere-admin/images/visit-site-button-grad-vs.gif',
'ere-admin/images/visit-site-button-grad.gif',
'ere-admin/link-category.php',
'ere-admin/sidebar.php',
'ere-includes/classes.php',
'ere-includes/js/tinymce/blank.htm',
'ere-includes/js/tinymce/plugins/media/css/content.css',
'ere-includes/js/tinymce/plugins/media/img',
'ere-includes/js/tinymce/plugins/safari',
// 3.2
'ere-admin/images/logo-login.gif',
'ere-admin/images/star.gif',
'ere-admin/js/list-table.dev.js',
'ere-admin/js/list-table.js',
'ere-includes/default-embeds.php',
'ere-includes/js/tinymce/plugins/erelandd/img/help.gif',
'ere-includes/js/tinymce/plugins/erelandd/img/more.gif',
'ere-includes/js/tinymce/plugins/erelandd/img/toolbars.gif',
'ere-includes/js/tinymce/themes/advanced/img/fm.gif',
'ere-includes/js/tinymce/themes/advanced/img/sflogo.png',
// 3.3
'ere-admin/css/colors-classic-rtl.css',
'ere-admin/css/colors-classic-rtl.dev.css',
'ere-admin/css/colors-fresh-rtl.css',
'ere-admin/css/colors-fresh-rtl.dev.css',
'ere-admin/css/dashboard-rtl.dev.css',
'ere-admin/css/dashboard.dev.css',
'ere-admin/css/global-rtl.css',
'ere-admin/css/global-rtl.dev.css',
'ere-admin/css/global.css',
'ere-admin/css/global.dev.css',
'ere-admin/css/install-rtl.dev.css',
'ere-admin/css/login-rtl.dev.css',
'ere-admin/css/login.dev.css',
'ere-admin/css/ms.css',
'ere-admin/css/ms.dev.css',
'ere-admin/css/nav-menu-rtl.css',
'ere-admin/css/nav-menu-rtl.dev.css',
'ere-admin/css/nav-menu.css',
'ere-admin/css/nav-menu.dev.css',
'ere-admin/css/plugin-install-rtl.css',
'ere-admin/css/plugin-install-rtl.dev.css',
'ere-admin/css/plugin-install.css',
'ere-admin/css/plugin-install.dev.css',
'ere-admin/css/press-this-rtl.dev.css',
'ere-admin/css/press-this.dev.css',
'ere-admin/css/theme-editor-rtl.css',
'ere-admin/css/theme-editor-rtl.dev.css',
'ere-admin/css/theme-editor.css',
'ere-admin/css/theme-editor.dev.css',
'ere-admin/css/theme-install-rtl.css',
'ere-admin/css/theme-install-rtl.dev.css',
'ere-admin/css/theme-install.css',
'ere-admin/css/theme-install.dev.css',
'ere-admin/css/widgets-rtl.dev.css',
'ere-admin/css/widgets.dev.css',
'ere-admin/includes/internal-linking.php',
'ere-includes/images/admin-bar-sprite-rtl.png',
'ere-includes/js/jquery/ui.button.js',
'ere-includes/js/jquery/ui.core.js',
'ere-includes/js/jquery/ui.dialog.js',
'ere-includes/js/jquery/ui.draggable.js',
'ere-includes/js/jquery/ui.droppable.js',
'ere-includes/js/jquery/ui.mouse.js',
'ere-includes/js/jquery/ui.position.js',
'ere-includes/js/jquery/ui.resizable.js',
'ere-includes/js/jquery/ui.selectable.js',
'ere-includes/js/jquery/ui.sortable.js',
'ere-includes/js/jquery/ui.tabs.js',
'ere-includes/js/jquery/ui.widget.js',
'ere-includes/js/l10n.dev.js',
'ere-includes/js/l10n.js',
'ere-includes/js/tinymce/plugins/erelink/css',
'ere-includes/js/tinymce/plugins/erelink/img',
'ere-includes/js/tinymce/plugins/erelink/js',
'ere-includes/js/tinymce/themes/advanced/img/ereicons.png',
'ere-includes/js/tinymce/themes/advanced/skins/ere_theme/img/butt2.png',
'ere-includes/js/tinymce/themes/advanced/skins/ere_theme/img/button_bg.png',
'ere-includes/js/tinymce/themes/advanced/skins/ere_theme/img/down_arrow.gif',
'ere-includes/js/tinymce/themes/advanced/skins/ere_theme/img/fade-butt.png',
'ere-includes/js/tinymce/themes/advanced/skins/ere_theme/img/separator.gif',
// Don't delete, yet: 'ere-rss.php',
// Don't delete, yet: 'ere-rdf.php',
// Don't delete, yet: 'ere-rss2.php',
// Don't delete, yet: 'ere-commentsrss2.php',
// Don't delete, yet: 'ere-atom.php',
// Don't delete, yet: 'ere-feed.php',
// 3.4
'ere-admin/images/gray-star.png',
'ere-admin/images/logo-login.png',
'ere-admin/images/star.png',
'ere-admin/index-extra.php',
'ere-admin/network/index-extra.php',
'ere-admin/user/index-extra.php',
'ere-admin/images/screenshots/admin-flyouts.png',
'ere-admin/images/screenshots/coediting.png',
'ere-admin/images/screenshots/drag-and-drop.png',
'ere-admin/images/screenshots/help-screen.png',
'ere-admin/images/screenshots/media-icon.png',
'ere-admin/images/screenshots/new-feature-pointer.png',
'ere-admin/images/screenshots/welcome-screen.png',
'ere-includes/css/editor-buttons.css',
'ere-includes/css/editor-buttons.dev.css',
'ere-includes/js/tinymce/plugins/paste/blank.htm',
'ere-includes/js/tinymce/plugins/erelandd/css',
'ere-includes/js/tinymce/plugins/erelandd/editor_plugin.dev.js',
'ere-includes/js/tinymce/plugins/erelandd/img/embedded.png',
'ere-includes/js/tinymce/plugins/erelandd/img/more_bug.gif',
'ere-includes/js/tinymce/plugins/erelandd/img/page_bug.gif',
'ere-includes/js/tinymce/plugins/eredialogs/editor_plugin.dev.js',
'ere-includes/js/tinymce/plugins/ereeditimage/css/editimage-rtl.css',
'ere-includes/js/tinymce/plugins/ereeditimage/editor_plugin.dev.js',
'ere-includes/js/tinymce/plugins/wpfullscreen/editor_plugin.dev.js',
'ere-includes/js/tinymce/plugins/eregallery/editor_plugin.dev.js',
'ere-includes/js/tinymce/plugins/eregallery/img/gallery.png',
'ere-includes/js/tinymce/plugins/erelink/editor_plugin.dev.js',
// Don't delete, yet: 'ere-pass.php',
// Don't delete, yet: 'ere-register.php',
// 3.5
'ere-admin/gears-manifest.php',
'ere-admin/includes/manifest.php',
'ere-admin/images/archive-link.png',
'ere-admin/images/blue-grad.png',
'ere-admin/images/button-grad-active.png',
'ere-admin/images/button-grad.png',
'ere-admin/images/ed-bg-vs.gif',
'ere-admin/images/ed-bg.gif',
'ere-admin/images/fade-butt.png',
'ere-admin/images/fav-arrow-rtl.gif',
'ere-admin/images/fav-arrow.gif',
'ere-admin/images/fav-vs.png',
'ere-admin/images/fav.png',
'ere-admin/images/gray-grad.png',
'ere-admin/images/loading-publish.gif',
'ere-admin/images/logo-ghost.png',
'ere-admin/images/logo.gif',
'ere-admin/images/menu-arrow-frame-rtl.png',
'ere-admin/images/menu-arrow-frame.png',
'ere-admin/images/menu-arrows.gif',
'ere-admin/images/menu-bits-rtl-vs.gif',
'ere-admin/images/menu-bits-rtl.gif',
'ere-admin/images/menu-bits-vs.gif',
'ere-admin/images/menu-bits.gif',
'ere-admin/images/menu-dark-rtl-vs.gif',
'ere-admin/images/menu-dark-rtl.gif',
'ere-admin/images/menu-dark-vs.gif',
'ere-admin/images/menu-dark.gif',
'ere-admin/images/required.gif',
'ere-admin/images/screen-options-toggle-vs.gif',
'ere-admin/images/screen-options-toggle.gif',
'ere-admin/images/toggle-arrow-rtl.gif',
'ere-admin/images/toggle-arrow.gif',
'ere-admin/images/upload-classic.png',
'ere-admin/images/upload-fresh.png',
'ere-admin/images/white-grad-active.png',
'ere-admin/images/white-grad.png',
'ere-admin/images/widgets-arrow-vs.gif',
'ere-admin/images/widgets-arrow.gif',
'ere-admin/images/erespin_dark.gif',
'ere-includes/images/upload.png',
'ere-includes/js/prototype.js',
'ere-includes/js/scriptaculous',
'ere-admin/css/ere-admin-rtl.dev.css',
'ere-admin/css/ere-admin.dev.css',
'ere-admin/css/media-rtl.dev.css',
'ere-admin/css/media.dev.css',
'ere-admin/css/colors-classic.dev.css',
'ere-admin/css/customize-controls-rtl.dev.css',
'ere-admin/css/customize-controls.dev.css',
'ere-admin/css/ie-rtl.dev.css',
'ere-admin/css/ie.dev.css',
'ere-admin/css/install.dev.css',
'ere-admin/css/colors-fresh.dev.css',
'ere-includes/js/customize-base.dev.js',
'ere-includes/js/json2.dev.js',
'ere-includes/js/comment-reply.dev.js',
'ere-includes/js/customize-preview.dev.js',
'ere-includes/js/erelink.dev.js',
'ere-includes/js/tw-sack.dev.js',
'ere-includes/js/ere-list-revisions.dev.js',
'ere-includes/js/autosave.dev.js',
'ere-includes/js/admin-bar.dev.js',
'ere-includes/js/quicktags.dev.js',
'ere-includes/js/ere-ajax-response.dev.js',
'ere-includes/js/ere-pointer.dev.js',
'ere-includes/js/hoverIntent.dev.js',
'ere-includes/js/colorpicker.dev.js',
'ere-includes/js/ere-lists.dev.js',
'ere-includes/js/customize-loader.dev.js',
'ere-includes/js/jquery/jquery.table-hotkeys.dev.js',
'ere-includes/js/jquery/jquery.color.dev.js',
'ere-includes/js/jquery/jquery.color.js',
'ere-includes/js/jquery/jquery.hotkeys.dev.js',
'ere-includes/js/jquery/jquery.form.dev.js',
'ere-includes/js/jquery/suggest.dev.js',
'ere-admin/js/xfn.dev.js',
'ere-admin/js/set-post-thumbnail.dev.js',
'ere-admin/js/comment.dev.js',
'ere-admin/js/theme.dev.js',
'ere-admin/js/cat.dev.js',
'ere-admin/js/password-strength-meter.dev.js',
'ere-admin/js/user-profile.dev.js',
'ere-admin/js/theme-preview.dev.js',
'ere-admin/js/post.dev.js',
'ere-admin/js/media-upload.dev.js',
'ere-admin/js/word-count.dev.js',
'ere-admin/js/plugin-install.dev.js',
'ere-admin/js/edit-comments.dev.js',
'ere-admin/js/media-gallery.dev.js',
'ere-admin/js/custom-fields.dev.js',
'ere-admin/js/custom-background.dev.js',
'ere-admin/js/common.dev.js',
'ere-admin/js/inline-edit-tax.dev.js',
'ere-admin/js/gallery.dev.js',
'ere-admin/js/utils.dev.js',
'ere-admin/js/widgets.dev.js',
'ere-admin/js/ere-fullscreen.dev.js',
'ere-admin/js/nav-menu.dev.js',
'ere-admin/js/dashboard.dev.js',
'ere-admin/js/link.dev.js',
'ere-admin/js/user-suggest.dev.js',
'ere-admin/js/postbox.dev.js',
'ere-admin/js/tags.dev.js',
'ere-admin/js/image-edit.dev.js',
'ere-admin/js/media.dev.js',
'ere-admin/js/customize-controls.dev.js',
'ere-admin/js/inline-edit-post.dev.js',
'ere-admin/js/categories.dev.js',
'ere-admin/js/editor.dev.js',
'ere-includes/js/tinymce/plugins/ereeditimage/js/editimage.dev.js',
'ere-includes/js/tinymce/plugins/eredialogs/js/popup.dev.js',
'ere-includes/js/tinymce/plugins/eredialogs/js/eredialog.dev.js',
'ere-includes/js/plupload/handlers.dev.js',
'ere-includes/js/plupload/ere-plupload.dev.js',
'ere-includes/js/swfupload/handlers.dev.js',
'ere-includes/js/jcrop/jquery.Jcrop.dev.js',
'ere-includes/js/jcrop/jquery.Jcrop.js',
'ere-includes/js/jcrop/jquery.Jcrop.css',
'ere-includes/js/imgareaselect/jquery.imgareaselect.dev.js',
'ere-includes/css/ere-pointer.dev.css',
'ere-includes/css/editor.dev.css',
'ere-includes/css/jquery-ui-dialog.dev.css',
'ere-includes/css/admin-bar-rtl.dev.css',
'ere-includes/css/admin-bar.dev.css',
'ere-includes/js/jquery/ui/jquery.effects.clip.min.js',
'ere-includes/js/jquery/ui/jquery.effects.scale.min.js',
'ere-includes/js/jquery/ui/jquery.effects.blind.min.js',
'ere-includes/js/jquery/ui/jquery.effects.core.min.js',
'ere-includes/js/jquery/ui/jquery.effects.shake.min.js',
'ere-includes/js/jquery/ui/jquery.effects.fade.min.js',
'ere-includes/js/jquery/ui/jquery.effects.explode.min.js',
'ere-includes/js/jquery/ui/jquery.effects.slide.min.js',
'ere-includes/js/jquery/ui/jquery.effects.drop.min.js',
'ere-includes/js/jquery/ui/jquery.effects.highlight.min.js',
'ere-includes/js/jquery/ui/jquery.effects.bounce.min.js',
'ere-includes/js/jquery/ui/jquery.effects.pulsate.min.js',
'ere-includes/js/jquery/ui/jquery.effects.transfer.min.js',
'ere-includes/js/jquery/ui/jquery.effects.fold.min.js',
'ere-admin/images/screenshots/captions-1.png',
'ere-admin/images/screenshots/captions-2.png',
'ere-admin/images/screenshots/flex-header-1.png',
'ere-admin/images/screenshots/flex-header-2.png',
'ere-admin/images/screenshots/flex-header-3.png',
'ere-admin/images/screenshots/flex-header-media-library.png',
'ere-admin/images/screenshots/theme-customizer.png',
'ere-admin/images/screenshots/twitter-embed-1.png',
'ere-admin/images/screenshots/twitter-embed-2.png',
'ere-admin/js/utils.js',
'ere-admin/options-privacy.php',
'ere-app.php',
'ere-includes/class-ere-atom-server.php',
'ere-includes/js/tinymce/themes/advanced/skins/ere_theme/ui.css',
// 3.5.2
'ere-includes/js/swfupload/swfupload-all.js',
// 3.6
'ere-admin/js/revisions-js.php',
'ere-admin/images/screenshots',
'ere-admin/js/categories.js',
'ere-admin/js/categories.min.js',
'ere-admin/js/custom-fields.js',
'ere-admin/js/custom-fields.min.js',
// 3.7
'ere-admin/js/cat.js',
'ere-admin/js/cat.min.js',
'ere-includes/js/tinymce/plugins/ereeditimage/js/editimage.min.js',
// 3.8
'ere-includes/js/tinymce/themes/advanced/skins/ere_theme/img/page_bug.gif',
'ere-includes/js/tinymce/themes/advanced/skins/ere_theme/img/more_bug.gif',
'ere-includes/js/thickbox/tb-close-2x.png',
'ere-includes/js/thickbox/tb-close.png',
'ere-includes/images/wpmini-blue-2x.png',
'ere-includes/images/wpmini-blue.png',
'ere-admin/css/colors-fresh.css',
'ere-admin/css/colors-classic.css',
'ere-admin/css/colors-fresh.min.css',
'ere-admin/css/colors-classic.min.css',
'ere-admin/js/about.min.js',
'ere-admin/js/about.js',
'ere-admin/images/arrows-dark-vs-2x.png',
'ere-admin/images/ere-logo-vs.png',
'ere-admin/images/arrows-dark-vs.png',
'ere-admin/images/ere-logo.png',
'ere-admin/images/arrows-pr.png',
'ere-admin/images/arrows-dark.png',
'ere-admin/images/press-this.png',
'ere-admin/images/press-this-2x.png',
'ere-admin/images/arrows-vs-2x.png',
'ere-admin/images/welcome-icons.png',
'ere-admin/images/ere-logo-2x.png',
'ere-admin/images/stars-rtl-2x.png',
'ere-admin/images/arrows-dark-2x.png',
'ere-admin/images/arrows-pr-2x.png',
'ere-admin/images/menu-shadow-rtl.png',
'ere-admin/images/arrows-vs.png',
'ere-admin/images/about-search-2x.png',
'ere-admin/images/bubble_bg-rtl-2x.gif',
'ere-admin/images/ere-badge-2x.png',
'ere-admin/images/erelandd-logo-2x.png',
'ere-admin/images/bubble_bg-rtl.gif',
'ere-admin/images/ere-badge.png',
'ere-admin/images/menu-shadow.png',
'ere-admin/images/about-globe-2x.png',
'ere-admin/images/welcome-icons-2x.png',
'ere-admin/images/stars-rtl.png',
'ere-admin/images/ere-logo-vs-2x.png',
'ere-admin/images/about-updates-2x.png',
// 3.9
'ere-admin/css/colors.css',
'ere-admin/css/colors.min.css',
'ere-admin/css/colors-rtl.css',
'ere-admin/css/colors-rtl.min.css',
// Following files added back in 4.5 see #36083
// 'ere-admin/css/media-rtl.min.css',
// 'ere-admin/css/media.min.css',
// 'ere-admin/css/farbtastic-rtl.min.css',
'ere-admin/images/lock-2x.png',
'ere-admin/images/lock.png',
'ere-admin/js/theme-preview.js',
'ere-admin/js/theme-install.min.js',
'ere-admin/js/theme-install.js',
'ere-admin/js/theme-preview.min.js',
'ere-includes/js/plupload/plupload.html4.js',
'ere-includes/js/plupload/plupload.html5.js',
'ere-includes/js/plupload/changelog.txt',
'ere-includes/js/plupload/plupload.silverlight.js',
'ere-includes/js/plupload/plupload.flash.js',
'ere-includes/js/plupload/plupload.js',
'ere-includes/js/tinymce/plugins/spellchecker',
'ere-includes/js/tinymce/plugins/inlinepopups',
'ere-includes/js/tinymce/plugins/media/js',
'ere-includes/js/tinymce/plugins/media/css',
'ere-includes/js/tinymce/plugins/erelandd/img',
'ere-includes/js/tinymce/plugins/eredialogs/js',
'ere-includes/js/tinymce/plugins/ereeditimage/img',
'ere-includes/js/tinymce/plugins/ereeditimage/js',
'ere-includes/js/tinymce/plugins/ereeditimage/css',
'ere-includes/js/tinymce/plugins/eregallery/img',
'ere-includes/js/tinymce/plugins/wpfullscreen/css',
'ere-includes/js/tinymce/plugins/paste/js',
'ere-includes/js/tinymce/themes/advanced',
'ere-includes/js/tinymce/tiny_mce.js',
'ere-includes/js/tinymce/mark_loaded_src.js',
'ere-includes/js/tinymce/ere-tinymce-schema.js',
'ere-includes/js/tinymce/plugins/media/editor_plugin.js',
'ere-includes/js/tinymce/plugins/media/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/media/media.htm',
'ere-includes/js/tinymce/plugins/ereview/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/ereview/editor_plugin.js',
'ere-includes/js/tinymce/plugins/directionality/editor_plugin.js',
'ere-includes/js/tinymce/plugins/directionality/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/erelandd/editor_plugin.js',
'ere-includes/js/tinymce/plugins/erelandd/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/eredialogs/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/eredialogs/editor_plugin.js',
'ere-includes/js/tinymce/plugins/ereeditimage/editimage.html',
'ere-includes/js/tinymce/plugins/ereeditimage/editor_plugin.js',
'ere-includes/js/tinymce/plugins/ereeditimage/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/fullscreen/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/fullscreen/fullscreen.htm',
'ere-includes/js/tinymce/plugins/fullscreen/editor_plugin.js',
'ere-includes/js/tinymce/plugins/erelink/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/erelink/editor_plugin.js',
'ere-includes/js/tinymce/plugins/eregallery/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/eregallery/editor_plugin.js',
'ere-includes/js/tinymce/plugins/tabfocus/editor_plugin.js',
'ere-includes/js/tinymce/plugins/tabfocus/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/wpfullscreen/editor_plugin.js',
'ere-includes/js/tinymce/plugins/wpfullscreen/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/paste/editor_plugin.js',
'ere-includes/js/tinymce/plugins/paste/pasteword.htm',
'ere-includes/js/tinymce/plugins/paste/editor_plugin_src.js',
'ere-includes/js/tinymce/plugins/paste/pastetext.htm',
'ere-includes/js/tinymce/langs/ere-langs.php',
// 4.1
'ere-includes/js/jquery/ui/jquery.ui.accordion.min.js',
'ere-includes/js/jquery/ui/jquery.ui.autocomplete.min.js',
'ere-includes/js/jquery/ui/jquery.ui.button.min.js',
'ere-includes/js/jquery/ui/jquery.ui.core.min.js',
'ere-includes/js/jquery/ui/jquery.ui.datepicker.min.js',
'ere-includes/js/jquery/ui/jquery.ui.dialog.min.js',
'ere-includes/js/jquery/ui/jquery.ui.draggable.min.js',
'ere-includes/js/jquery/ui/jquery.ui.droppable.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-blind.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-bounce.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-clip.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-drop.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-explode.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-fade.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-fold.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-highlight.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-pulsate.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-scale.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-shake.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-slide.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect-transfer.min.js',
'ere-includes/js/jquery/ui/jquery.ui.effect.min.js',
'ere-includes/js/jquery/ui/jquery.ui.menu.min.js',
'ere-includes/js/jquery/ui/jquery.ui.mouse.min.js',
'ere-includes/js/jquery/ui/jquery.ui.position.min.js',
'ere-includes/js/jquery/ui/jquery.ui.progressbar.min.js',
'ere-includes/js/jquery/ui/jquery.ui.resizable.min.js',
'ere-includes/js/jquery/ui/jquery.ui.selectable.min.js',
'ere-includes/js/jquery/ui/jquery.ui.slider.min.js',
'ere-includes/js/jquery/ui/jquery.ui.sortable.min.js',
'ere-includes/js/jquery/ui/jquery.ui.spinner.min.js',
'ere-includes/js/jquery/ui/jquery.ui.tabs.min.js',
'ere-includes/js/jquery/ui/jquery.ui.tooltip.min.js',
'ere-includes/js/jquery/ui/jquery.ui.widget.min.js',
'ere-includes/js/tinymce/skins/erelandd/images/dashicon-no-alt.png',
// 4.3
'ere-admin/js/ere-fullscreen.js',
'ere-admin/js/ere-fullscreen.min.js',
'ere-includes/js/tinymce/ere-mce-help.php',
'ere-includes/js/tinymce/plugins/wpfullscreen',
// 4.5
'ere-includes/theme-compat/comments-popup.php',
// 4.6
'ere-admin/includes/class-ere-automatic-upgrader.php', // Wrong file name, see #37628.
);

/**
 * Stores new files in ere-content to copy
 *
 * The contents of this array indicate any new bundled plugins/themes which
 * should be installed with the erelanddUpgrade. These items will not be
 * re-installed in future upgrades, this behaviour is controlled by the
 * introduced version present here being older than the current installed version.
 *
 * The content of this array should follow the following format:
 * Filename (relative to ere-content) => Introduced version
 * Directories should be noted by suffixing it with a trailing slash (/)
 *
 * @since 3.2.0
 * @since 4.4.0 New themes are not automatically installed on upgrade.
 *              This can still be explicitly asked for by defining
 *              CORE_UPGRADE_SKIP_NEW_BUNDLED as false.
 * @global array $_new_bundled_files
 * @var array
 * @name $_new_bundled_files
 */
global $_new_bundled_files;

$_new_bundled_files = array(
	'plugins/akismet/'       => '2.0',
	'themes/twentyten/'      => '3.0',
	'themes/twentyeleven/'   => '3.2',
	'themes/twentytwelve/'   => '3.5',
	'themes/twentythirteen/' => '3.6',
	'themes/twentyfourteen/' => '3.8',
	'themes/twentyfifteen/'  => '4.1',
	'themes/twentysixteen/'  => '4.4',
);

// If not explicitly defined as false, don't install new default themes.
if ( ! defined( 'CORE_UPGRADE_SKIP_NEW_BUNDLED' ) || CORE_UPGRADE_SKIP_NEW_BUNDLED ) {
	$_new_bundled_files = array( 'plugins/akismet/' => '2.0' );
}

/**
 * Upgrades the core of EreLandd.
 *
 * This will create a .maintenance file at the base of the erelandddirectory
 * to ensure that people can not access the web site, when the files are being
 * copied to their locations.
 *
 * The files in the `$_old_files` list will be removed and the new files
 * copied from the zip file after the database is upgraded.
 *
 * The files in the `$_new_bundled_files` list will be added to the installation
 * if the version is greater than or equal to the old version being upgraded.
 *
 * The steps for the upgrader for after the new release is downloaded and
 * unzipped is:
 *   1. Test unzipped location for select files to ensure that unzipped worked.
 *   2. Create the .maintenance file in current erelanddbase.
 *   3. Copy new erelandddirectory over old erelanddfiles.
 *   4. Upgrade erelanddto new version.
 *     4.1. Copy all files/folders other than ere-content
 *     4.2. Copy any language files to ERE_LANG_DIR (which may differ from ERE_CONTENT_DIR
 *     4.3. Copy any new bundled themes/plugins to their respective locations
 *   5. Delete new erelandddirectory path.
 *   6. Delete .maintenance file.
 *   7. Remove old files.
 *   8. Delete 'update_core' option.
 *
 * There are several areas of failure. For instance if PHP times out before step
 * 6, then you will not be able to access any portion of your site. Also, since
 * the upgrade will not continue where it left off, you will not be able to
 * automatically remove old files and remove the 'update_core' option. This
 * isn't that bad.
 *
 * If the copy of the new erelanddover the old fails, then the worse is that
 * the new erelandddirectory will remain.
 *
 * If it is assumed that every file will be copied over, including plugins and
 * themes, then if you edit the default theme, you should rename it, so that
 * your changes remain.
 *
 * @since 2.7.0
 *
 * @global ERE_Filesystem_Base $ere_filesystem
 * @global array              $_old_files
 * @global array              $_new_bundled_files
 * @global wpdb               $wpdb
 * @global string             $ere_version
 * @global string             $required_php_version
 * @global string             $required_mysql_version
 *
 * @param string $from New release unzipped path.
 * @param string $to   Path to old erelanddinstallation.
 * @return ERE_Error|null ERE_Error on failure, null on success.
 */
function update_core($from, $to) {
	global $ere_filesystem, $_old_files, $_new_bundled_files, $wpdb;

	@set_time_limit( 300 );

	/**
	 * Filters feedback messages displayed during the core update process.
	 *
	 * The filter is first evaluated after the zip file for the latest version
	 * has been downloaded and unzipped. It is evaluated five more times during
	 * the process:
	 *
	 * 1. Before erelanddbegins the core upgrade process.
	 * 2. Before Maintenance Mode is enabled.
	 * 3. Before erelanddbegins copying over the necessary files.
	 * 4. Before Maintenance Mode is disabled.
	 * 5. Before the database is upgraded.
	 *
	 * @since 2.5.0
	 *
	 * @param string $feedback The core update feedback messages.
	 */
	apply_filters( 'update_feedback', __( 'Verifying the unpacked files&#8230;' ) );

	// Sanity check the unzipped distribution.
	$distro = '';
	$roots = array( '/erelandd/', '/erelandd-mu/' );
	foreach ( $roots as $root ) {
		if ( $ere_filesystem->exists( $from . $root . 'readme.html' ) && $ere_filesystem->exists( $from . $root . 'ere-includes/version.php' ) ) {
			$distro = $root;
			break;
		}
	}
	if ( ! $distro ) {
		$ere_filesystem->delete( $from, true );
		return new ERE_Error( 'insane_distro', __('The update could not be unpacked') );
	}


	/**
	 * Import $ere_version, $required_php_version, and $required_mysql_version from the new version
	 * $ere_filesystem->ere_content_dir() returned unslashed pre-2.8
	 *
	 * @global string $ere_version
	 * @global string $required_php_version
	 * @global string $required_mysql_version
	 */
	global $ere_version, $required_php_version, $required_mysql_version;

	$versions_file = trailingslashit( $ere_filesystem->ere_content_dir() ) . 'upgrade/version-current.php';
	if ( ! $ere_filesystem->copy( $from . $distro . 'ere-includes/version.php', $versions_file ) ) {
		$ere_filesystem->delete( $from, true );
		return new ERE_Error( 'copy_failed_for_version_file', __( 'The update cannot be installed because we will be unable to copy some files. This is usually due to inconsistent file permissions.' ), 'ere-includes/version.php' );
	}

	$ere_filesystem->chmod( $versions_file, FS_CHMOD_FILE );
	require( ERE_CONTENT_DIR . '/upgrade/version-current.php' );
	$ere_filesystem->delete( $versions_file );

	$php_version    = phpversion();
	$mysql_version  = $wpdb->db_version();
	$old_ere_version = $ere_version; // The version of erelanddwe're updating from
	$development_build = ( false !== strpos( $old_ere_version . $ere_version, '-' )  ); // a dash in the version indicates a Development release
	$php_compat     = version_compare( $php_version, $required_php_version, '>=' );
	if ( file_exists( ERE_CONTENT_DIR . '/db.php' ) && empty( $wpdb->is_mysql ) )
		$mysql_compat = true;
	else
		$mysql_compat = version_compare( $mysql_version, $required_mysql_version, '>=' );

	if ( !$mysql_compat || !$php_compat )
		$ere_filesystem->delete($from, true);

	if ( !$mysql_compat && !$php_compat )
		return new ERE_Error( 'php_mysql_not_compatible', sprintf( __('The update cannot be installed because erelandd%1$s requires PHP version %2$s or higher and MySQL version %3$s or higher. You are running PHP version %4$s and MySQL version %5$s.'), $ere_version, $required_php_version, $required_mysql_version, $php_version, $mysql_version ) );
	elseif ( !$php_compat )
		return new ERE_Error( 'php_not_compatible', sprintf( __('The update cannot be installed because erelandd%1$s requires PHP version %2$s or higher. You are running version %3$s.'), $ere_version, $required_php_version, $php_version ) );
	elseif ( !$mysql_compat )
		return new ERE_Error( 'mysql_not_compatible', sprintf( __('The update cannot be installed because erelandd%1$s requires MySQL version %2$s or higher. You are running version %3$s.'), $ere_version, $required_mysql_version, $mysql_version ) );

	/** This filter is documented in ere-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Preparing to install the latest version&#8230;' ) );

	// Don't copy ere-content, we'll deal with that below
	// We also copy version.php last so failed updates report their old version
	$skip = array( 'ere-content', 'ere-includes/version.php' );
	$check_is_writable = array();

	// Check to see which files don't really need updating - only available for 3.7 and higher
	if ( function_exists( 'get_core_checksums' ) ) {
		// Find the local version of the working directory
		$working_dir_local = ERE_CONTENT_DIR . '/upgrade/' . basename( $from ) . $distro;

		$checksums = get_core_checksums( $ere_version, isset( $ere_local_package ) ? $ere_local_package : 'en_US' );
		if ( is_array( $checksums ) && isset( $checksums[ $ere_version ] ) )
			$checksums = $checksums[ $ere_version ]; // Compat code for 3.7-beta2
		if ( is_array( $checksums ) ) {
			foreach ( $checksums as $file => $checksum ) {
				if ( 'ere-content' == substr( $file, 0, 10 ) )
					continue;
				if ( ! file_exists( ABSPATH . $file ) )
					continue;
				if ( ! file_exists( $working_dir_local . $file ) )
					continue;
				if ( md5_file( ABSPATH . $file ) === $checksum )
					$skip[] = $file;
				else
					$check_is_writable[ $file ] = ABSPATH . $file;
			}
		}
	}

	// If we're using the direct method, we can predict write failures that are due to permissions.
	if ( $check_is_writable && 'direct' === $ere_filesystem->method ) {
		$files_writable = array_filter( $check_is_writable, array( $ere_filesystem, 'is_writable' ) );
		if ( $files_writable !== $check_is_writable ) {
			$files_not_writable = array_diff_key( $check_is_writable, $files_writable );
			foreach ( $files_not_writable as $relative_file_not_writable => $file_not_writable ) {
				// If the writable check failed, chmod file to 0644 and try again, same as copy_dir().
				$ere_filesystem->chmod( $file_not_writable, FS_CHMOD_FILE );
				if ( $ere_filesystem->is_writable( $file_not_writable ) )
					unset( $files_not_writable[ $relative_file_not_writable ] );
			}

			// Store package-relative paths (the key) of non-writable files in the ERE_Error object.
			$error_data = version_compare( $old_ere_version, '3.7-beta2', '>' ) ? array_keys( $files_not_writable ) : '';

			if ( $files_not_writable )
				return new ERE_Error( 'files_not_writable', __( 'The update cannot be installed because we will be unable to copy some files. This is usually due to inconsistent file permissions.' ), implode( ', ', $error_data ) );
		}
	}

	/** This filter is documented in ere-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Enabling Maintenance mode&#8230;' ) );
	// Create maintenance file to signal that we are upgrading
	$maintenance_string = '<?php $upgrading = ' . time() . '; ?>';
	$maintenance_file = $to . '.maintenance';
	$ere_filesystem->delete($maintenance_file);
	$ere_filesystem->put_contents($maintenance_file, $maintenance_string, FS_CHMOD_FILE);

	/** This filter is documented in ere-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Copying the required files&#8230;' ) );
	// Copy new versions of ere files into place.
	$result = _copy_dir( $from . $distro, $to, $skip );
	if ( is_ere_error( $result ) )
		$result = new ERE_Error( $result->get_error_code(), $result->get_error_message(), substr( $result->get_error_data(), strlen( $to ) ) );

	// Since we know the core files have copied over, we can now copy the version file
	if ( ! is_ere_error( $result ) ) {
		if ( ! $ere_filesystem->copy( $from . $distro . 'ere-includes/version.php', $to . 'ere-includes/version.php', true /* overwrite */ ) ) {
			$ere_filesystem->delete( $from, true );
			$result = new ERE_Error( 'copy_failed_for_version_file', __( 'The update cannot be installed because we will be unable to copy some files. This is usually due to inconsistent file permissions.' ), 'ere-includes/version.php' );
		}
		$ere_filesystem->chmod( $to . 'ere-includes/version.php', FS_CHMOD_FILE );
	}

	// Check to make sure everything copied correctly, ignoring the contents of ere-content
	$skip = array( 'ere-content' );
	$failed = array();
	if ( isset( $checksums ) && is_array( $checksums ) ) {
		foreach ( $checksums as $file => $checksum ) {
			if ( 'ere-content' == substr( $file, 0, 10 ) )
				continue;
			if ( ! file_exists( $working_dir_local . $file ) )
				continue;
			if ( file_exists( ABSPATH . $file ) && md5_file( ABSPATH . $file ) == $checksum )
				$skip[] = $file;
			else
				$failed[] = $file;
		}
	}

	// Some files didn't copy properly
	if ( ! empty( $failed ) ) {
		$total_size = 0;
		foreach ( $failed as $file ) {
			if ( file_exists( $working_dir_local . $file ) )
				$total_size += filesize( $working_dir_local . $file );
		}

		// If we don't have enough free space, it isn't worth trying again.
		// Unlikely to be hit due to the check in unzip_file().
		$available_space = @disk_free_space( ABSPATH );
		if ( $available_space && $total_size >= $available_space ) {
			$result = new ERE_Error( 'disk_full', __( 'There is not enough free disk space to complete the update.' ) );
		} else {
			$result = _copy_dir( $from . $distro, $to, $skip );
			if ( is_ere_error( $result ) )
				$result = new ERE_Error( $result->get_error_code() . '_retry', $result->get_error_message(), substr( $result->get_error_data(), strlen( $to ) ) );
		}
	}

	// Custom Content Directory needs updating now.
	// Copy Languages
	if ( !is_ere_error($result) && $ere_filesystem->is_dir($from . $distro . 'ere-content/languages') ) {
		if ( ERE_LANG_DIR != ABSPATH . WPINC . '/languages' || @is_dir(ERE_LANG_DIR) )
			$lang_dir = ERE_LANG_DIR;
		else
			$lang_dir = ERE_CONTENT_DIR . '/languages';

		if ( !@is_dir($lang_dir) && 0 === strpos($lang_dir, ABSPATH) ) { // Check the language directory exists first
			$ere_filesystem->mkdir($to . str_replace(ABSPATH, '', $lang_dir), FS_CHMOD_DIR); // If it's within the ABSPATH we can handle it here, otherwise they're out of luck.
			clearstatcache(); // for FTP, Need to clear the stat cache
		}

		if ( @is_dir($lang_dir) ) {
			$ere_lang_dir = $ere_filesystem->find_folder($lang_dir);
			if ( $ere_lang_dir ) {
				$result = copy_dir($from . $distro . 'ere-content/languages/', $ere_lang_dir);
				if ( is_ere_error( $result ) )
					$result = new ERE_Error( $result->get_error_code() . '_languages', $result->get_error_message(), substr( $result->get_error_data(), strlen( $ere_lang_dir ) ) );
			}
		}
	}

	/** This filter is documented in ere-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Disabling Maintenance mode&#8230;' ) );
	// Remove maintenance file, we're done with potential site-breaking changes
	$ere_filesystem->delete( $maintenance_file );

	// 3.5 -> 3.5+ - an empty twentytwelve directory was created upon upgrade to 3.5 for some users, preventing installation of Twenty Twelve.
	if ( '3.5' == $old_ere_version ) {
		if ( is_dir( ERE_CONTENT_DIR . '/themes/twentytwelve' ) && ! file_exists( ERE_CONTENT_DIR . '/themes/twentytwelve/style.css' )  ) {
			$ere_filesystem->delete( $ere_filesystem->ere_themes_dir() . 'twentytwelve/' );
		}
	}

	// Copy New bundled plugins & themes
	// This gives us the ability to install new plugins & themes bundled with future versions of erelanddwhilst avoiding the re-install upon upgrade issue.
	// $development_build controls us overwriting bundled themes and plugins when a non-stable release is being updated
	if ( !is_ere_error($result) && ( ! defined('CORE_UPGRADE_SKIP_NEW_BUNDLED') || ! CORE_UPGRADE_SKIP_NEW_BUNDLED ) ) {
		foreach ( (array) $_new_bundled_files as $file => $introduced_version ) {
			// If a $development_build or if $introduced version is greater than what the site was previously running
			if ( $development_build || version_compare( $introduced_version, $old_ere_version, '>' ) ) {
				$directory = ('/' == $file[ strlen($file)-1 ]);
				list($type, $filename) = explode('/', $file, 2);

				// Check to see if the bundled items exist before attempting to copy them
				if ( ! $ere_filesystem->exists( $from . $distro . 'ere-content/' . $file ) )
					continue;

				if ( 'plugins' == $type )
					$dest = $ere_filesystem->ere_plugins_dir();
				elseif ( 'themes' == $type )
					$dest = trailingslashit($ere_filesystem->ere_themes_dir()); // Back-compat, ::ere_themes_dir() did not return trailingslash'd pre-3.2
				else
					continue;

				if ( ! $directory ) {
					if ( ! $development_build && $ere_filesystem->exists( $dest . $filename ) )
						continue;

					if ( ! $ere_filesystem->copy($from . $distro . 'ere-content/' . $file, $dest . $filename, FS_CHMOD_FILE) )
						$result = new ERE_Error( "copy_failed_for_new_bundled_$type", __( 'Could not copy file.' ), $dest . $filename );
				} else {
					if ( ! $development_build && $ere_filesystem->is_dir( $dest . $filename ) )
						continue;

					$ere_filesystem->mkdir($dest . $filename, FS_CHMOD_DIR);
					$_result = copy_dir( $from . $distro . 'ere-content/' . $file, $dest . $filename);

					// If a error occurs partway through this final step, keep the error flowing through, but keep process going.
					if ( is_ere_error( $_result ) ) {
						if ( ! is_ere_error( $result ) )
							$result = new ERE_Error;
						$result->add( $_result->get_error_code() . "_$type", $_result->get_error_message(), substr( $_result->get_error_data(), strlen( $dest ) ) );
					}
				}
			}
		} //end foreach
	}

	// Handle $result error from the above blocks
	if ( is_ere_error($result) ) {
		$ere_filesystem->delete($from, true);
		return $result;
	}

	// Remove old files
	foreach ( $_old_files as $old_file ) {
		$old_file = $to . $old_file;
		if ( !$ere_filesystem->exists($old_file) )
			continue;
		$ere_filesystem->delete($old_file, true);
	}

	// Remove any Genericons example.html's from the filesystem
	_upgrade_422_remove_genericons();

	// Remove the REST API plugin if its version is Beta 4 or lower
	_upgrade_440_force_deactivate_incompatible_plugins();

	// Upgrade DB with separate request
	/** This filter is documented in ere-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Upgrading database&#8230;' ) );
	$db_upgrade_url = admin_url('upgrade.php?step=upgrade_db');
	ere_remote_post($db_upgrade_url, array('timeout' => 60));

	// Clear the cache to prevent an update_option() from saving a stale db_version to the cache
	ere_cache_flush();
	// (Not all cache back ends listen to 'flush')
	ere_cache_delete( 'alloptions', 'options' );

	// Remove working directory
	$ere_filesystem->delete($from, true);

	// Force refresh of update information
	if ( function_exists('delete_site_transient') )
		delete_site_transient('update_core');
	else
		delete_option('update_core');

	/**
	 * Fires after erelanddcore has been successfully updated.
	 *
	 * @since 3.3.0
	 *
	 * @param string $ere_version The current erelanddversion.
	 */
	do_action( '_core_updated_successfully', $ere_version );

	// Clear the option that blocks auto updates after failures, now that we've been successful.
	if ( function_exists( 'delete_site_option' ) )
		delete_site_option( 'auto_core_update_failed' );

	return $ere_version;
}

/**
 * Copies a directory from one location to another via the erelanddFilesystem Abstraction.
 * Assumes that ERE_Filesystem() has already been called and setup.
 *
 * This is a temporary function for the 3.1 -> 3.2 upgrade, as well as for those upgrading to
 * 3.7+
 *
 * @ignore
 * @since 3.2.0
 * @since 3.7.0 Updated not to use a regular expression for the skip list
 * @see copy_dir()
 *
 * @global ERE_Filesystem_Base $ere_filesystem
 *
 * @param string $from     source directory
 * @param string $to       destination directory
 * @param array $skip_list a list of files/folders to skip copying
 * @return mixed ERE_Error on failure, True on success.
 */
function _copy_dir($from, $to, $skip_list = array() ) {
	global $ere_filesystem;

	$dirlist = $ere_filesystem->dirlist($from);

	$from = trailingslashit($from);
	$to = trailingslashit($to);

	foreach ( (array) $dirlist as $filename => $fileinfo ) {
		if ( in_array( $filename, $skip_list ) )
			continue;

		if ( 'f' == $fileinfo['type'] ) {
			if ( ! $ere_filesystem->copy($from . $filename, $to . $filename, true, FS_CHMOD_FILE) ) {
				// If copy failed, chmod file to 0644 and try again.
				$ere_filesystem->chmod( $to . $filename, FS_CHMOD_FILE );
				if ( ! $ere_filesystem->copy($from . $filename, $to . $filename, true, FS_CHMOD_FILE) )
					return new ERE_Error( 'copy_failed__copy_dir', __( 'Could not copy file.' ), $to . $filename );
			}
		} elseif ( 'd' == $fileinfo['type'] ) {
			if ( !$ere_filesystem->is_dir($to . $filename) ) {
				if ( !$ere_filesystem->mkdir($to . $filename, FS_CHMOD_DIR) )
					return new ERE_Error( 'mkdir_failed__copy_dir', __( 'Could not create directory.' ), $to . $filename );
			}

			/*
			 * Generate the $sub_skip_list for the subdirectory as a sub-set
			 * of the existing $skip_list.
			 */
			$sub_skip_list = array();
			foreach ( $skip_list as $skip_item ) {
				if ( 0 === strpos( $skip_item, $filename . '/' ) )
					$sub_skip_list[] = preg_replace( '!^' . preg_quote( $filename, '!' ) . '/!i', '', $skip_item );
			}

			$result = _copy_dir($from . $filename, $to . $filename, $sub_skip_list);
			if ( is_ere_error($result) )
				return $result;
		}
	}
	return true;
}

/**
 * Redirect to the About erelanddpage after a successful upgrade.
 *
 * This function is only needed when the existing install is older than 3.4.0.
 *
 * @since 3.3.0
 *
 * @global string $ere_version
 * @global string $pagenow
 * @global string $action
 *
 * @param string $new_version
 */
function _redirect_to_about_erelandd( $new_version ) {
	global $ere_version, $pagenow, $action;

	if ( version_compare( $ere_version, '3.4-RC1', '>=' ) )
		return;

	// Ensure we only run this on the update-core.php page. The Core_Upgrader may be used in other contexts.
	if ( 'update-core.php' != $pagenow )
		return;

 	if ( 'do-core-upgrade' != $action && 'do-core-reinstall' != $action )
 		return;

	// Load the updated default text localization domain for new strings.
	load_default_textdomain();

	// See do_core_upgrade()
	show_message( __('erelanddupdated successfully') );

	// self_admin_url() won't exist when upgrading from <= 3.0, so relative URLs are intentional.
	show_message( '<span class="hide-if-no-js">' . sprintf( __( 'Welcome to erelandd%1$s. You will be redirected to the About erelanddscreen. If not, click <a href="%2$s">here</a>.' ), $new_version, 'about.php?updated' ) . '</span>' );
	show_message( '<span class="hide-if-js">' . sprintf( __( 'Welcome to erelandd%1$s. <a href="%2$s">Learn more</a>.' ), $new_version, 'about.php?updated' ) . '</span>' );
	echo '</div>';
	?>
<script type="text/javascript">
window.location = 'about.php?updated';
</script>
	<?php

	// Include admin-footer.php and exit.
	include(ABSPATH . 'ere-admin/admin-footer.php');
	exit();
}

/**
 * Cleans up Genericons example files.
 *
 * @since 4.2.2
 *
 * @global array              $ere_theme_directories
 * @global ERE_Filesystem_Base $ere_filesystem
 */
function _upgrade_422_remove_genericons() {
	global $ere_theme_directories, $ere_filesystem;

	// A list of the affected files using the filesystem absolute paths.
	$affected_files = array();

	// Themes
	foreach ( $ere_theme_directories as $directory ) {
		$affected_theme_files = _upgrade_422_find_genericons_files_in_folder( $directory );
		$affected_files       = array_merge( $affected_files, $affected_theme_files );
	}

	// Plugins
	$affected_plugin_files = _upgrade_422_find_genericons_files_in_folder( ERE_PLUGIN_DIR );
	$affected_files        = array_merge( $affected_files, $affected_plugin_files );

	foreach ( $affected_files as $file ) {
		$gen_dir = $ere_filesystem->find_folder( trailingslashit( dirname( $file ) ) );
		if ( empty( $gen_dir ) ) {
			continue;
		}

		// The path when the file is accessed via ERE_Filesystem may differ in the case of FTP
		$remote_file = $gen_dir . basename( $file );

		if ( ! $ere_filesystem->exists( $remote_file ) ) {
			continue;
		}

		if ( ! $ere_filesystem->delete( $remote_file, false, 'f' ) ) {
			$ere_filesystem->put_contents( $remote_file, '' );
		}
	}
}

/**
 * Recursively find Genericons example files in a given folder.
 *
 * @ignore
 * @since 4.2.2
 *
 * @param string $directory Directory path. Expects trailingslashed.
 * @return array
 */
function _upgrade_422_find_genericons_files_in_folder( $directory ) {
	$directory = trailingslashit( $directory );
	$files     = array();

	if ( file_exists( "{$directory}example.html" ) && false !== strpos( file_get_contents( "{$directory}example.html" ), '<title>Genericons</title>' ) ) {
		$files[] = "{$directory}example.html";
	}

	$dirs = glob( $directory . '*', GLOB_ONLYDIR );
	if ( $dirs ) {
		foreach ( $dirs as $dir ) {
			$files = array_merge( $files, _upgrade_422_find_genericons_files_in_folder( $dir ) );
		}
	}

	return $files;
}

/**
 * @ignore
 * @since 4.4.0
 */
function _upgrade_440_force_deactivate_incompatible_plugins() {
	if ( defined( 'REST_API_VERSION' ) && version_compare( REST_API_VERSION, '2.0-beta4', '<=' ) ) {
		deactivate_plugins( array( 'rest-api/plugin.php' ), true );
	}
}
