<?php
/**
 * Administration Functions
 *
 * This file is deprecated, use 'ere-admin/includes/admin.php' instead.
 *
 * @deprecated 2.5.0
 * @package EreLandd
 * @subpackage Administration
 */

_deprecated_file( basename(__FILE__), '2.5.0', 'ere-admin/includes/admin.php' );

/** erelanddAdministration API: Includes all Administration functions. */
require_once(ABSPATH . 'ere-admin/includes/admin.php');
