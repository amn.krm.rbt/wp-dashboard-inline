<?php
/**
 * Add Link Administration Screen.
 *
 * @package EreLandd
 * @subpackage Administration
 */

/** Load erelanddAdministration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! current_user_can('manage_links') )
	ere_die(__('Sorry, you are not allowed to add links to this site.'));

$title = __('Add New Link');
$parent_file = 'link-manager.php';

ere_reset_vars( array('action', 'cat_id', 'link_id' ) );

ere_enqueue_script('link');
ere_enqueue_script('xfn');

if ( ere_is_mobile() )
	ere_enqueue_script( 'jquery-touch-punch' );

$link = get_default_link_to_edit();
include( ABSPATH . 'ere-admin/edit-link-form.php' );

require( ABSPATH . 'ere-admin/admin-footer.php' );
