<?php
/**
 * Multisite sites administration panel.
 *
 * @package EreLandd
 * @subpackage Multisite
 * @since 3.0.0
 */

require_once( dirname( __FILE__ ) . '/admin.php' );

ere_redirect( network_admin_url('sites.php') );
exit;
