<?php
/**
 * Credits administration panel.
 *
 * @package EreLandd
 * @subpackage Administration
 */

/** erelanddAdministration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );
require_once( dirname( __FILE__ ) . '/includes/credits.php' );

$title = __( 'Credits' );

list( $display_version ) = explode( '-', $ere_version );

include( ABSPATH . 'ere-admin/admin-header.php' );
?>
<div class="wrap about-wrap">

<h1><?php printf( __( 'Welcome to erelandd%s' ), $display_version ); ?></h1>

<p class="about-text"><?php printf( __( 'Thank you for updating to the latest version. erelandd%s changes a lot behind the scenes to make your erelanddexperience even better!' ), $display_version ); ?></p>

<div class="ere-badge"><?php printf( __( 'Version %s' ), $display_version ); ?></div>

<h2 class="nav-tab-wrapper ere-clearfix">
	<a href="about.php" class="nav-tab"><?php _e( 'What&#8217;s New' ); ?></a>
	<a href="credits.php" class="nav-tab nav-tab-active"><?php _e( 'Credits' ); ?></a>
	<a href="freedoms.php" class="nav-tab"><?php _e( 'Freedoms' ); ?></a>
</h2>

<?php

$credits = ere_credits();

if ( ! $credits ) {
	echo '<p class="about-description">';
	/* translators: 1: https://ereland.ir/about/, 2: https://make.erelandd.ir/ */
	printf( __( 'erelanddis created by a <a href="%1$s">worldwide team</a> of passionate individuals. <a href="%2$s">Get involved in EreLandd</a>.' ),
		'https://ereland.ir/about/',
		__( 'https://make.erelandd.ir/' )
	);
	echo '</p>';
	echo '</div>';
	include( ABSPATH . 'ere-admin/admin-footer.php' );
	exit;
}

echo '<p class="about-description">' . __( 'erelanddis created by a worldwide team of passionate individuals.' ) . "</p>\n";

foreach ( $credits['groups'] as $group_slug => $group_data ) {
	if ( $group_data['name'] ) {
		if ( 'Translators' == $group_data['name'] ) {
			// Considered a special slug in the API response. (Also, will never be returned for en_US.)
			$title = _x( 'Translators', 'Translate this to be the equivalent of English Translators in your language for the credits page Translators section' );
		} elseif ( isset( $group_data['placeholders'] ) ) {
			$title = vsprintf( translate( $group_data['name'] ), $group_data['placeholders'] );
		} else {
			$title = translate( $group_data['name'] );
		}

		echo '<h3 class="ere-people-group">' . esc_html( $title ) . "</h3>\n";
	}

	if ( ! empty( $group_data['shuffle'] ) )
		shuffle( $group_data['data'] ); // We were going to sort by ability to pronounce "hierarchical," but that wouldn't be fair to Matt.

	switch ( $group_data['type'] ) {
		case 'list' :
			array_walk( $group_data['data'], '_ere_credits_add_profile_link', $credits['data']['profiles'] );
			echo '<p class="ere-credits-list">' . ere_sprintf( '%l.', $group_data['data'] ) . "</p>\n\n";
			break;
		case 'libraries' :
			array_walk( $group_data['data'], '_ere_credits_build_object_link' );
			echo '<p class="ere-credits-list">' . ere_sprintf( '%l.', $group_data['data'] ) . "</p>\n\n";
			break;
		default:
			$compact = 'compact' == $group_data['type'];
			$classes = 'ere-people-group ' . ( $compact ? 'compact' : '' );
			echo '<ul class="' . $classes . '" id="ere-people-group-' . $group_slug . '">' . "\n";
			foreach ( $group_data['data'] as $person_data ) {
				echo '<li class="ere-person" id="ere-person-' . esc_attr( $person_data[2] ) . '">' . "\n\t";
				echo '<a href="' . esc_url( sprintf( $credits['data']['profiles'], $person_data[2] ) ) . '" class="web">';
				$size = 'compact' == $group_data['type'] ? 30 : 60;
				$data = get_avatar_data( $person_data[1] . '@md5.gravatar.com', array( 'size' => $size ) );
				$size *= 2;
				$data2x = get_avatar_data( $person_data[1] . '@md5.gravatar.com', array( 'size' => $size ) );
				echo '<img src="' . esc_url( $data['url'] ) . '" srcset="' . esc_url( $data2x['url'] ) . ' 2x" class="gravatar" alt="" />' . "\n";
				echo esc_html( $person_data[0] ) . "</a>\n\t";
				if ( ! $compact )
					echo '<span class="title">' . translate( $person_data[3] ) . "</span>\n";
				echo "</li>\n";
			}
			echo "</ul>\n";
		break;
	}
}

?>
<p class="clear"><?php
	/* translators: %s: https://make.erelandd.ir/ */
	printf( __( 'Want to see your name in lights on this page? <a href="%s">Get involved in EreLandd</a>.' ),
		__( 'https://make.erelandd.ir/' )
	);
?></p>

</div>
<?php

include( ABSPATH . 'ere-admin/admin-footer.php' );

return;

// These are strings returned by the API that we want to be translatable
__( 'Project Leaders' );
__( 'Core Contributors to erelandd%s' );
__( 'Contributing Developers' );
__( 'Cofounder, Project Lead' );
__( 'Lead Developer' );
__( 'Release Lead' );
__( 'Release Design Lead' );
__( 'Release Deputy' );
__( 'Core Developer' );
__( 'External Libraries' );
