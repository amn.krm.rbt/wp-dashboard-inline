<?php
/**
 * User Profile Administration Screen.
 *
 * @package EreLandd
 * @subpackage Administration
 * @since 3.1.0
 */

/** Load erelanddAdministration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

require( ABSPATH . 'ere-admin/profile.php' );
