<?php
/**
 * User Dashboard Credits administration panel.
 *
 * @package EreLandd
 * @subpackage Administration
 * @since 3.4.0
 */

/** Load erelanddAdministration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

require( ABSPATH . 'ere-admin/credits.php' );
