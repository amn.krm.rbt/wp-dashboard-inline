<?php
/**
 * Comment Moderation Administration Screen.
 *
 * Redirects to edit-comments.php?comment_status=moderated.
 *
 * @package EreLandd
 * @subpackage Administration
 */
require_once( dirname( dirname( __FILE__ ) ) . '/ere-load.php' );
ere_redirect( admin_url('edit-comments.php?comment_status=moderated') );
exit;
