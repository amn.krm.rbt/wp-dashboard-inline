<?php
/**
 * erelanddUpgrade Functions. Old file, must not be used. Include
 * ere-admin/includes/upgrade.php instead.
 *
 * @deprecated 2.5.0
 * @package EreLandd
 * @subpackage Administration
 */

_deprecated_file( basename(__FILE__), '2.5.0', 'ere-admin/includes/upgrade.php' );
require_once(ABSPATH . 'ere-admin/includes/upgrade.php');
